#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# auth: S.Morales, B.Salvachua
# date: 2019-06-05

import os,sys,glob
import matplotlib
import matplotlib.pyplot as plt
import pytimber
import time
import pandas
import csv
import numpy as np
from datetime import datetime,timedelta
from scipy.optimize import curve_fit
import math
import blmtime
import blmdb

class BlmData:
    def __init__(self, rs = 9, blmTypes=["IC","SEM","LIC"] , beamModes=["INJPROT", "NOBEAM", "SETUP","CYCLING"], fileBlmDcum = "lsa/20200213_BlmDbLsa.csv"):
        # Input parameters
        # Selected BLM running sum, integer number from 1 to 12.
        self.RS = rs
        self.useVectorNumericBlm = True

        self.fig_size = (5,3)

        # blmTypes list that can be: [IC, LIC or SEM ]
        self.blmTypes = [ i for i in blmTypes ]

        # Running Sum Times
        self.rs_time_s = [ 0.04e-3, 0.08e-3, 0.32e-3, 0.64e-3, 2.56e-3, 10.24e-3, 81.92e-3, 655.36e-3, 1310.72e-3, 5242.88e-3, 20971.52e-3, 83886.08e-3] 

        # Conversion Factor from Grays to BITS
        self.blmGraysToBits = {"IC" : 3.62e-9/self.rs_time_s[self.RS-1], "LIC" : 2.17e-7/self.rs_time_s[self.RS-1], "SEM" : 2.53e-4/self.rs_time_s[self.RS-1],"DUMP_IC" : 3.62e-9/self.rs_time_s[self.RS-1], "DUMP_LIC" : 2.17e-7/self.rs_time_s[self.RS-1], "DUMP_SEM" : 2.53e-4/self.rs_time_s[self.RS-1] , "UNKNOWN": 1}
        #print(self.blmGraysToBits["IC"])
        #print(self.rs_time_s[self.RS-1])

        self.ShowPlots = False
        
        # FOR LIC
        # Sara: Correct LIC factors for previous years??
        #self.blmbit = 2.17e-7/self.rs_time_s[self.RS-1]
        #self.blmbit = 5.07e-8/40e-9 # new factor since later 2016, 14 times higher than IC
        #wrong factor for LIC, Mar 2016 to Nov. 2016 3.86e-6

        # Selected Beam Mode to find the data without beam
        self.beamModes = [ i for i in beamModes]
        self.currentBeamMode = None
        
        print( "Initialized to RS%02d" %(self.RS),  self.blmTypes, self.beamModes)        
               
        self._secondsInDay = 86400
        self._secondsInHour = 3600
        self.outputFilename = None
        self.output_dir_path = "../BlmNoiseData"
        
        #Flag to decide if we filter the BLMs to perform the analysis or not
        #self.filter = filter

        self.fig_counter = 0
        self._userBlmList = []
        self.fileBlmDcum = fileBlmDcum

        try:
            self.db
        except AttributeError:
            self.db = pytimber.LoggingDB()    
        return

    def _getBlmNameDcumFromFile(self, filename = "/lsa/20200213_BlmDbLsa.csv"):
        #print(self._userBlmList)
        self.blmName, self.blmDcum = blmdb.get_blm_lsa_name_dcum_from_file( filename, self._userBlmList, self.blmTypes, True)
        self.blmDcumDict = { blm:dcum for blm,dcum in zip( self.blmName, self.blmDcum )}
        self.blmName = list(self.blmName)
        self.blmDcum = list(self.blmDcum)
        return self.blmName, self.blmDcum

    def _get_vme_host_name(self, crate_cmw_name ):
        #expect HC.BLM.SR1.C -> return CFV-SR1-BLMC
        vme_host_name = "CFV-"+crate_cmw_name.split(".")[2]+"-BLM"+crate_cmw_name.split(".")[3]
        return vme_host_name 


    def _setVariableNames(self):
        # Vector Numeric variables
        self.varVector_IC  = "LHC.BLMI:LOSS_RS" + '%02d' % (self.RS)
        self.varVector_SEM = "LHC.BLMS:LOSS_RS" + '%02d' % (self.RS)
        # Numeric Variables
        self.blmLossVar    = [ name+":LOSS_RS"+'%02d' % (self.RS) for name in self.blmName ]
        self.beamPresentB1 = 'HX:SMP1_PRESENT'
        self.beamPresentB2 = 'HX:SMP2_PRESENT'
        self.beamIntensityB1 = 'LHC.BCTFR.A6R4.B1:BEAM_INTENSITY'
        # Combine Numeric variables
        self.variables_noBlm = [self.beamPresentB1 , self.beamPresentB2 , self.beamIntensityB1] 
        ####self.variables = self.blmLossVar + self.variables_noBlm
        self.variables  = [ var for var in self.blmLossVar ]
        self.variables += [ var for var in self.variables_noBlm ] 
        return 
    
    def getIntervalsByLHCModes(self, t1, t2, mode1, unixtime=True,
                               mode1time='startTime', mode2time='endTime'):
        ts1 = self.db.toTimestamp(t1)
        ts2 = self.db.toTimestamp(t2)
        fills = self.db.getLHCFillsByTime(ts1, ts2, mode1)
        out = []
        for fill in fills:
            fn=fill['fillNumber']
            m1=[]
            m2=[]
            for bm in fill['beamModes']:
                if bm['mode'] == mode1:
                    m1.append(bm[mode1time])
                    m2.append(bm[mode2time])
            if len(m1) > 0 and len(m2) > 0:
                out.append([fn, m1, m2])
        return out

    def _getBlmMetadata(self, varNameVector, t1):
        t1_sec  = t1
        meta_data= self.db.getMetaData( varNameVector )
        tt, vv = meta_data[ varNameVector ]
        for i in range(len( tt )):
            if tt[i] <= t1_sec: selected_index = i

        ## Correcting 2018 HEADER: OK only difference in 2018-04-07 00:00:00            
        blmHeader = [ name for name in vv[selected_index] ]
        return blmHeader


    def getData(self, t1, t2, blmList = [], mode = "Aligned", rs = None):
        if rs == None:
            rs = self.RS
        self.RS = rs
        print("RS%02d" %(self.RS))
        self._userBlmList = [ i for i in blmList ]
        self._getBlmNameDcumFromFile(self.fileBlmDcum)
        self._setVariableNames()

        t1 = blmtime.timeFromStr( t1 )
        t2 = blmtime.timeFromStr( t2 )

        # mode coulde be "Aligned" or "Scaled". "Scaled is extremly slow"
        print("Getting data for ",blmtime.strFromTime( t1 ), " to ",blmtime.strFromTime( t2 ) )# , "now", datetime.now())

        allvariables = []
        if self.useVectorNumericBlm == True: 
            allvariables = [i for i in self.variables_noBlm]
        else:
            allvariables = [i for i in self.variables]

        databkg1 = {}
        if mode == "Aligned":
            print("Extracting",mode,"data")
            try:
                databkg1 = self.db.getAligned( allvariables , t1, t2, master = self.beamIntensityB1, unixtime=True)                 
            except AttributeError:
                print("No data found in range for selected variables")
                return [],{},[]
        elif mode == "Scaled":
            print("Extracting",mode,"data")
            databkg1=self.db.getScaled( allvariables,t1,t2,unixtime=True,scaleAlgorithm='REPEAT',scaleInterval='SECOND',scaleSize='1')   
               
        print("Data downloaded...")
        # Remove the variables that have not data in the logging DB        
        for i in allvariables:
            try:
                databkg1[i]
            except KeyError:
                var_name = i
                blm_name = var_name.split(":")[0]
                #print("[Warning]: Variable not in CALS ", var_name )
                self.blmDcum.pop( self.blmName.index(blm_name) )
                self.blmName.remove( blm_name )
                self.variables.remove( var_name)
                #self.variables_noBlm.remove( var_name)
                self.blmLossVar.remove( var_name )
                continue

        allvariables = []
        if self.useVectorNumericBlm == True: 
            allvariables = [i for i in self.variables_noBlm]
        else:
            allvariables = [i for i in self.variables]

        # Prepare the extracted data variables (times and values)
        tt_data = None
        vv_data = None
        if mode == "Aligned":
            tt_data =  databkg1['timestamps']
            vv_data = { var : databkg1[var] for var in allvariables}
        elif mode == "Scaled":
            # use the time of the first variable
            tt_data =  databkg1[ self.variables_noBlm[0] ][0]
            vv_data = { var : databkg1[var][1] for var in allvariables }            

        sample_size = len( tt_data )
        if self.useVectorNumericBlm == True:
            print("Using VectorNumeric Data IC and SEM")
            # Get List of BLMs
            blmHeaderVector_ic    = self._getBlmMetadata( self.varVector_IC,  t1 )
            blmHeaderVector_sem   = self._getBlmMetadata( self.varVector_SEM, t1 )
            print("Size MetaData IC:" ,  len(blmHeaderVector_ic))
            print("Size MetaData SEM:" , len(blmHeaderVector_sem))

            if len(blmList) == 0:
                print("Filling blmList with VectorNumeric Metadata (IC and SEM)")
                blmList = [ blm for blm in blmHeaderVector_ic ]        
                blmList += [ blm for blm in blmHeaderVector_sem ]
                
            # Get BLMI vector numeric data (only BLM data), IC
            blmDataVector_ic  = self.db.get(self.varVector_IC,  t1,t2, unixtime = True)
            tt_blmDataVector , vv_blmDataVector = blmDataVector_ic[self.varVector_IC]
            if len(tt_blmDataVector) != len(tt_data ): print("WARNING: size VECTORNUM IC",len(tt_blmDataVector) , " size NUM ", len(tt_data ))
            sample_size = min( len(tt_blmDataVector) , len(tt_data ) )
            for i in range(len(blmHeaderVector_ic)):
                for selected_blm in blmList:
                    if blmHeaderVector_ic[i] == selected_blm:
                        vv_data[ selected_blm+":LOSS_RS%02d" %(self.RS) ] = [ vv_blmDataVector[it][i] for it in range(sample_size) ]

            # Get BLMS vector numeric data (only BLM data), SEM
            blmDataVector_sem     = self.db.get(self.varVector_SEM,  t1,t2, unixtime = True)
            tt_blmDataVector , vv_blmDataVector = blmDataVector_sem[self.varVector_SEM]
            if len(tt_blmDataVector) != len(tt_data ): print("WARNING: size VECTORNUM SEM",len(tt_blmDataVector) , " size NUM ", len(tt_data ))
            sample_size = min( len(tt_blmDataVector) , len(tt_data ) )
            for i in range(len(blmHeaderVector_sem)):
                for selected_blm in blmList:
                    if blmHeaderVector_sem[i] in selected_blm:
                        vv_data[ selected_blm+":LOSS_RS%02d" %(self.RS) ] = [ vv_blmDataVector[it][i] for it in range(sample_size) ]

        if len(blmList) == 0:
            print("blmList zero after vectornumeric")
            blmList = [ var.split(":")[0] for var in self.blmLossVar ]
        # check all data is of the same length, if problem return empyt data
        print("Initial number of samples: ", sample_size)
        for ikey in list(vv_data.keys()):
            if abs( sample_size - len(vv_data[ikey])) > 2 :
                print("ERROR: data is not correctly sampled, expected to be ",
                      sample_size, " and is  ", len(vv_data[ikey]), " for ", ikey, "returning empty data_bkg")
                return [],{},[]

        # re-size the output data
        for key in vv_data:
            vv_data[key] = vv_data[key][0:sample_size]
        tt_data = tt_data[0:sample_size]
        vv_data["time"] = tt_data[0:sample_size]

        print("Finished data extraction")
        return tt_data, vv_data, blmList


    def correctNaNvalues(self,vv_data, blmList):
        variablenan = 0
        for selected_blm in blmList:
            ### correct nan data
            #loop, if nan take j-1
            for j in range(len(vv_data[ selected_blm+":LOSS_RS%02d" %(self.RS) ])):
                if math.isnan(vv_data[ selected_blm+":LOSS_RS%02d" %(self.RS) ][j]):
                    variablenan = 1
                    vv_data[ selected_blm+":LOSS_RS%02d" %(self.RS) ][j] = vv_data[ selected_blm+":LOSS_RS%02d" %(self.RS) ][j-1]


        print("En este periodo ha habido NaN: ", variablenan)

        return vv_data



    def _getBlmType(self,blmName):
        bitBlmType = blmName[4]
        if bitBlmType == "I": 
            if blmName[3] == "D":
                return "DUMP_IC"
            else:
                return "IC"
        elif bitBlmType == "S":
            if blmName[3] == "D":
                return "DUMP_SEM"
            else: 
                return "SEM"
        elif bitBlmType == "L": 
            if blmName[3] == "D":
                return "DUMP_LIC"
            else:
                return "LIC"            
        else: 
            print("WARNING: unknown type", blmName, bitBlmType)
            return "UNKNOWN"

    def _getDcum(self, blm ):
        try:
            return self.blmDcumDict[blm]
        except KeyError:
            #print("ERROR: %s does not exists in DCUM file, result will be -1" %(blm))
            return -1

    def plotVsTime(self, blmList, t1, t2, rs=9, progressBar = []):
        try:
            progressBar.setMaximum(len(blmList) + 3) 
            progressBar.setValue(1)
        except AttributeError:
            pass

        tt_data, vv_data, blmList2 = self.getData(t1, t2, blmList = blmList, mode = "Aligned", rs = rs)
        try:
            progressBar.setValue(2)
        except AttributeError:
            pass

        if len(blmList) > 25:
            print("ERROR: Too many BLM selected (",len(blmList), " limit is 25.")
            return
        self.fig_counter +=1 
        plt.figure(self.fig_counter)
        plt.title("Running Sum RS%02d" %(self.RS))
        for blm in blmList:
            var = blm+":LOSS_RS%02d" %(self.RS)
            plt.plot(tt_data, vv_data[var], "-o", label=blm)
            try:
                progressBar.setValue(progressBar.value() + 1)
            except AttributeError:
                pass
        plt.legend()
        plt.ylabel("BLM signal (Gy/s)")
        #plt.xlabel("Time")
        pytimber.set_xaxis_date()
        plt.ticklabel_format(axis='y', style='sci', scilimits=(-3,3))
        plt.tight_layout()
        if self.ShowPlots == True : plt.show()
        try:
            progressBar.setValue(progressBar.value() + 1)
        except AttributeError:
            pass

        plt.close('all')
        return
        
if __name__ == "__main__":
    obj = BlmData( rs = 9)
    obj.ShowPlots = True
    obj.useVectorNumericBlm = True
    t1 = "2018-10-04 21:45:00"
    t2 = "2018-10-04 21:49:00"
    blmList = ['BLMEI.05L7.B2I10_TCSM.A5L7.B2', 'BLMQI.04L7.B2I10_MQWA.E4L7']
    tt, vv, blmList = obj.getData( t1, t2, blmList)
    print( len(tt) )
    print( blmList )
    print( vv.keys() )
    obj.plotVsTime(blmList, t1,t2, rs = 9)

