# BLM Signal Analysis

**Scripts and files for BLM Signal Analysis**

The main BLM Signal Analysis scripts are **blmnoise.py**, which contains the code that generates the BLM noise files and **blmdose.py**, which contains the code that generates the BLM dose files. Additionally, both scripts contain the corresponding codes for the analysis of the data in the files.

*Note*: The CERN Logging Database is only accessible if connected to the Technical Network inside CERN. Therefore, it is necessary to be connected to the CERN Network to perform a data extraction.


Access to the noise files from:
- 2015: https://cernbox.cern.ch/index.php/s/v1qMfGeJw8RuRds
- 2016: https://cernbox.cern.ch/index.php/s/8Pletk9JGndrLdM
- 2017: https://cernbox.cern.ch/index.php/s/mHylhJKk2kAbQRU
- 2018: https://cernbox.cern.ch/index.php/s/LjFx47e4TyLyRjB

Access to the dose files from:
- 2015: https://cernbox.cern.ch/index.php/s/dqRRWYAvxAMRPHT
- 2016: https://cernbox.cern.ch/index.php/s/W2iY1QQd27pQIUA
- 2017: https://cernbox.cern.ch/index.php/s/rAuJyn1gVABNnzr
- 2018: https://cernbox.cern.ch/index.php/s/mw7uWk7wMXkMLEl
