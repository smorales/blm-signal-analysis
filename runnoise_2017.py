import blmnoise
import blmtime

# MANY blm MIGHT NOT EXIST...
"""
t0 = "2017-05-01 00:00:00" # + 230 days
#t0 = "2017-12-04 00:00:00" # 
t_final = "2017-12-20 00:00:00" # 
for i in range(230):
    t1 = blmtime.strFromTime( blmtime.timeFromStr( t0 ) + i*24*3600)
    t2 = blmtime.strFromTime( blmtime.timeFromStr( t0 ) + (i+1)*24*3600)
    if blmtime.timeFromStr(t2) > blmtime.timeFromStr(t_final):
        sys.exit()
    print("***************************")
    print(t1, t2)
    obj = blmnoise.BlmNoise( rs = 9, beamModes=["INJPROT", "NOBEAM", "SETUP","CYCLING"])
    obj.ShowPlots = False
    obj.output_dir_path = "../BlmNoiseDataCorrected_2017"
    obj.useVectorNumericBlm = True 
    obj.runNoiseAnalysisDuringPeriod( t1, t2 )
    print("***************************\n")
"""

obj = blmnoise.BlmNoise( rs = 9, beamModes=["INJPROT", "NOBEAM", "SETUP","CYCLING"])
obj.output_dir_path = "../BlmNoiseDataCorrected_2017"
t0 = "2017-05-01 00:00:00"
t1 = "2017-05-02 00:00:00"
obj._getBlmNoiseFileList(t0,t1)
