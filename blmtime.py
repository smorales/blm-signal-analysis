#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# auth: S.Morales, B.Salvachua
# date: 2019-11-07

import time

def timeFromStr(timeStr):
    #Accepts two different string formats
    try:
        return time.mktime(time.strptime(timeStr, "%Y-%m-%d %H:%M:%S"))
    except ValueError:
        return time.mktime(time.strptime(timeStr, "%Y-%m-%d"))
        
def strFromTime(timeSec):
    return time.strftime("%Y-%m-%d %H:%M:%S", time.localtime(timeSec) )

def strForFilenameFromTime(timeSec):
    return time.strftime("%Y%m%d_%H%M%S", time.localtime(timeSec) )

def timeFromFilenameFromStr(timeStr):
    return time.mktime( time.strptime(timeStr, "%Y%m%d_%H%M%S"))

import pytz
import datetime as dt
def convertLocal2UTC(t_str):
    local = pytz.timezone("Europe/Amsterdam")
    naive = dt.datetime.strptime (t_str, "%Y-%m-%d %H:%M:%S.%f")
    local_dt = local.localize(naive, is_dst=None)
    utc_dt = local_dt.astimezone(pytz.utc)
    return utc_dt.strftime("%Y-%m-%d %H:%M:%S.%f")[0:-3]

def convertUTC2Local(t_str):
    local = pytz.timezone("Europe/Amsterdam")
    naive = dt.datetime.strptime (t_str, "%Y-%m-%d %H:%M:%S.%f")
    utc_dt =naive.replace(tzinfo=pytz.UTC)
    local_dt = utc_dt.astimezone(local)
    return local_dt.strftime("%Y-%m-%d %H:%M:%S.%f")[0:-3]
    
def convertTimestamp2Str(t_timestamp):
    local = pytz.timezone("Europe/Amsterdam")
    naive = dt.datetime.fromtimestamp(t_timestamp)
    local_dt = local.localize(naive, is_dst=None)
    return local_dt.strftime("%Y-%m-%d %H:%M:%S.%f")[0:-3]
