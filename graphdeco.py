
import matplotlib.transforms as transforms
import matplotlib.pyplot as plt

vlines = { "IP" : [] }
vlines["IP"].append( [ 0.000      , "IP1" , "k", "--"] )
vlines["IP"].append( [ 3332.3604  , "IP2" , "k", "--"] )
vlines["IP"].append( [ 6664.7208  , "IP3" , "k", "--"] )
vlines["IP"].append( [ 9997.0812  , "IP4" , "k", "--"] )
vlines["IP"].append( [ 13329.4416 , "IP5" , "k", "--"] )
vlines["IP"].append( [ 16661.802  , "IP6" , "k", "--"] )
vlines["IP"].append( [ 19994.1624 , "IP7" , "k", "--"])
vlines["IP"].append( [ 23315.3028 , "IP8" , "k", "--"] )


def VerticalLinesIP(ax):
    alpha_general = 0.1
    color_general = "purple"
    vertical_pos  = 0.8
    trans = transforms.blended_transform_factory(ax.transData, ax.transAxes)
    ax.axvline( 0.000, alpha = alpha_general, color = color_general )
    ax.text(0.000, vertical_pos, "IP1", color = color_general, transform = trans )
    ax.axvline( 26658.8832, alpha = alpha_general, color = color_general )
    ax.text(26658.8832, vertical_pos, "IP1L1", color = color_general, transform = trans )

    ax.axvline( 3332.3604, alpha = alpha_general, color = color_general )
    ax.text(3332.3604, vertical_pos, "IP2", color = color_general, transform = trans )

    ax.axvline( 6664.7208, alpha = alpha_general, color = color_general )
    ax.text(6664.7208 , vertical_pos, "IP3", color = color_general, transform = trans )

    ax.axvline( 9997.0812 , alpha = alpha_general, color = color_general )
    ax.text(9997.0812  , vertical_pos, "IP4", color = color_general, transform = trans )

    ax.axvline( 13329.4416, alpha = alpha_general, color = color_general )
    ax.text( 13329.4416, vertical_pos, "IP5", color = color_general, transform = trans )

    ax.axvline( 16661.802, alpha = alpha_general, color = color_general )
    ax.text( 16661.802, vertical_pos, "IP6", color = color_general, transform = trans )

    ax.axvline( 19994.1624 , alpha = alpha_general, color = color_general )
    ax.text(19994.1624  , vertical_pos, "IP7", color = color_general, transform = trans )

    ax.axvline( 23315.3028, alpha = alpha_general, color = color_general )
    ax.text( 23315.3028, vertical_pos, "IP8", color = color_general, transform = trans )

    return

def GetMadElements(filenameb1 = 'twiss/twiss_lhcb1.tfs', filenameb2 = 'twiss/twiss_lhcb2.tfs'):
    twissDict = { "NAME" : [], "KEYWORD" : [], "S" : [], "L" : [] }
    header = []
    data_format = []
    keepMagnet = True
    for filename in [filenameb1, filenameb2]:
        reader = open(filename, "r")
        for row in reader:
            if "@" in row: continue
            if len(header) == 0: 
                header = row.split()
                continue
            if len(data_format) == 0:
                data_format = row.split() 
                continue
            row = row.replace('"',"")
            tmpDict = { key:val for key,val in zip(header[1:], row.split() ) }
            if tmpDict["NAME"] == "IP1":  start_lhc = float(tmpDict["S"])
            if tmpDict["KEYWORD"] in ["SBEND", "QUADRUPOLE"] and keepMagnet == True:
                twissDict["NAME"] += [ str(tmpDict["NAME"])[:-3]]
                twissDict["KEYWORD"] += [ str(tmpDict["KEYWORD"])]
                twissDict["S"]    += [ float(tmpDict["S"])]
                twissDict["L"]    += [ float(tmpDict["L"])]
            if tmpDict["KEYWORD"] in ["RCOLLIMATOR"]:
                twissDict["NAME"] += [ str(tmpDict["NAME"])]
                twissDict["KEYWORD"] += [ str(tmpDict["KEYWORD"])]
                twissDict["S"]    += [ float(tmpDict["S"])]
                twissDict["L"]    += [ float(tmpDict["L"])]
        keepMagnet = False
        reader.close()
    
    for i in range(len( twissDict["S"])):
        twissDict["S"][i] = twissDict["S"][i] - start_lhc
        if twissDict["S"][i] < 0:
            twissDict["S"][i] = 26658.8832 + twissDict["S"][i]
    return twissDict
    

def MadElementsVerticalRectangle(ax, twissDataDict):
    from matplotlib.patches import Rectangle
    from matplotlib.collections import PatchCollection
    rec_bend = []
    rec_coll = []
    rec_quad = []
    tex = []
    #trans = transforms.blended_transform_factory(ax.transData, ax.transAxes)
    qcolor = 'blue'
    ymin, ymax = ax.get_ylim()
    import math
    region = ( math.log10(ymax) - math.log10(ymin) )*0.9
    y = math.pow( 10, math.log10(ymin) + region)
    h = ymax - y
    for i in range(len(twissDataDict["NAME"])):
        name = twissDataDict["NAME"][i] 
        key  = twissDataDict["KEYWORD"][i]
        spos = twissDataDict["S"][i]
        lel  = twissDataDict["L"][i]
        # SPOS from twiss at the END of the element
        x = spos - lel
        w = lel
        if "SBEND" in key:
            rec_bend += [ Rectangle( (x,y), w, h ) ]
            ax.add_artist(rec_bend[-1] )
            rx, ry = rec_bend[-1].get_xy()
            cx = rx + rec_bend[-1].get_width()/2
            cy = ry + rec_bend[-1].get_height()/2
            #ax.annotate(name, (cx,cy), color="black", weight="bold", fontsize=3, ha="center", va="center")
            ax.text( cx, cy, name, fontsize = 5, rotation = 90 )

        elif "QUADRUPOLE" in key:
            rec_quad += [ Rectangle( (x,y), w, h ) ]
        elif "RCOLLIMATOR" in key:
            rec_coll += [ Rectangle( (x,y), w, h ) ]
    #ax.add_collection(  PatchCollection( rec_bend, facecolor = qcolor, alpha = 0.5, edgecolor = "blue"))
    ax.add_collection( PatchCollection( rec_bend, facecolor = "blue",  alpha = 0.5, edgecolor = "blue"))
    ax.add_collection( PatchCollection( rec_quad, facecolor = "green", alpha = 0.5, edgecolor = "green"))
    ax.add_collection( PatchCollection( rec_coll, facecolor = "black", alpha = 0.5, edgecolor = "black"))
    return
