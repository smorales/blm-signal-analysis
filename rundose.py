import blmdose
import blmtime

t0 = "2017-04-30 00:00:00"
for i in range(1):
    t1 = blmtime.strFromTime( blmtime.timeFromStr( t0 ) + i*24*3600)
    t2 = blmtime.strFromTime( blmtime.timeFromStr( t0 ) + (i+1)*24*3600)
    print("***************************")
    print(t1, t2)

    obj = blmdose.BlmDose( rs = 9)
    obj.output_dir_path = "../Dose_2017"
    obj.noise_dir_path = "../BlmNoiseDataCorrected_2017"
    #obj.energy = 2500
    obj.ShowPlots = False
    obj.runDoseAnalysisDuringPeriod( t1, t2 )
    print("***************************\n")
