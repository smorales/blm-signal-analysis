#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# auth: S.Morales, B.Salvachua
# date: 2019-06-05

import os,sys, glob
import matplotlib.pyplot as plt
import matplotlib
import pytimber
import time
import pandas
import csv
import numpy as np
from datetime import datetime,timedelta
from collections import defaultdict
from sklearn.linear_model import LinearRegression
import operator

from blmdata import BlmData
import blmtime
import blmdb
import graphdeco as gd


class BlmDose(BlmData):
    def __init__(self, energy = 6400, rs = 9, blmTypes=["IC","SEM","LIC"] , beamModes=["FLATTOP","SQUEEZE","ADJUST","STABLE"], fileBlmDcum = "lsa/20200213_BlmDbLsa.csv"):
        BlmData.__init__(self, rs, blmTypes,  beamModes, fileBlmDcum )
        self.energy = energy
        self.output_dir_path = "../BlmDoseData"
        self.noise_dir_path = "../BlmNoiseDataCorrected"
        self.output_files = "../TFG"
    
    def _setVariableNames(self):
        # Vector Numeric variables
        self.varVector_IC  = "LHC.BLMI:LOSS_RS" + '%02d' % (self.RS)
        self.varVector_SEM = "LHC.BLMS:LOSS_RS" + '%02d' % (self.RS)
        # Numeric Variables
        self.blmLossVar = [ name+":LOSS_RS"+'%02d' % (self.RS) for name in self.blmName ]
        self.beamPresentB1 = 'HX:SMP1_PRESENT'
        self.beamPresentB2 = 'HX:SMP2_PRESENT'
        self.lumiAlice = 'ALICE:LUMI_TOT_INST'
        self.lumiAtlas = 'ATLAS:LUMI_TOT_INST'
        self.lumiCMS = 'CMS:LUMI_TOT_INST'
        self.lumiLHCb = 'LHCB:LUMI_TOT_INST'
        self.beamIntensityB1 = 'LHC.BCTFR.A6R4.B1:BEAM_INTENSITY'
        self.beamIntensityB2 = 'LHC.BCTFR.A6R4.B2:BEAM_INTENSITY'
        self.beamEnergyB1 = 'LHC.BSRA.US45.B1:ABORT_GAP_ENERGY'
        self.beamEnergyB2 = 'LHC.BSRA.US45.B2:ABORT_GAP_ENERGY'
        # Combine Numeric Variables
        self.variables_noBlm = [self.lumiAlice, self.lumiAtlas, self.lumiCMS , self.lumiLHCb,self.beamIntensityB1,self.beamIntensityB2, self.beamEnergyB1, self.beamEnergyB2, self.beamPresentB1 , self.beamPresentB2]

    def runDoseAnalysisDuringPeriod( self, t1, t2, userblmList = [] ):
        print("**********************************")
        print("******* Run Dose Analysis  *******")
        print(t1, " -  ",t2)
               
        for imode in self.beamModes:
            fills = self.getIntervalsByLHCModes(t1, t2 , imode, unixtime=True, mode1time='startTime', mode2time='endTime')
            if len(fills) == 0:
                continue
            else:
                self.currentBeamMode = imode

            for fill,it1s,it2s in fills:
                self.file_counter = 0
                for period in range(len(it1s)):
                    it1 = it1s[period]
                    it2 = it2s[period]
                    print("* ",self.currentBeamMode," : fill " ,fill, " from ", blmtime.strFromTime( it1 ), " to ",blmtime.strFromTime( it2 ) )
                    while it2 > it1:
                        mint2 = min(it2, it1 + self._secondsInHour)
                        str_t1 = blmtime.strFromTime(it1)
                        str_t2 = blmtime.strFromTime(mint2)
                        # Extract data in blocks of 1hour
                        tt, vv, blmList = self.getData( str_t1, str_t2 , userblmList)  
                        self._filterDataBeamPresent( tt, vv, blmList )
                        if len(self.data_beam_present["time"]) == 0: 
                            it1 =  it1 + self._secondsInHour
                            continue
                        self._readBkg( str_t1 )
                        self._sumData()
                        # write temporal output file
                        self.current_fill    = fill
                        self.current_start   = blmtime.strForFilenameFromTime(it1)
                        self.current_end     = blmtime.strForFilenameFromTime(mint2)
                        filename = self.output_dir_path+'/tmp_BlmDose_%s_%s_fill_%d_bm_%s_RS%02d_%d.csv' %( self.current_start, self.current_end, self.current_fill, self.currentBeamMode, self.RS, self.file_counter )
                        self._writeFile(str_t1, str_t2, filename)
                        it1 =  it1 + self._secondsInHour
                        self.file_counter += 1

                
                self._mergeFilesPerFillBeamMode( fill, imode , self.RS)
        return

    def _mergeFilesPerFillBeamMode(self, fill, mode, rs):
        selectedPattern = self.output_dir_path+"/tmp_BlmDose*fill_%d*bm_%s*RS%02d*.csv" %(fill, mode , rs)
        print(selectedPattern)
        selectedList = glob.glob(selectedPattern)
        selectedList.sort()
        [ print(f) for f in selectedList ]
        if len(selectedList) == 0:
            print("ERROR: No files to merge in ",self.output_dir_path)
            return

        # Create Pandas DataFrame of concat files
        df = []
        for f in selectedList:
            df.append( pandas.read_csv(f))

        
        # Accumulate all values in the df[0]
    
        for i in range(1, len( selectedList )):
            df[0]["BlmDose"] += df[i]["BlmDose"]
            df[0]["TotLumiAlice"]+= df[i]["TotLumiAlice"] 
            df[0]["TotLumiAtlas"]+= df[i]["TotLumiAtlas"]
            df[0]["TotLumiCMS"]  += df[i]["TotLumiCMS"]
            df[0]["TotLumiLHCb"] += df[i]["TotLumiLHCb"]
            df[0]["TotIntB1"]    = max( df[0]["TotIntB1"][0], df[i]["TotIntB1"][0])
            df[0]["TotIntB2"]    = max( df[0]["TotIntB2"][0], df[i]["TotIntB2"][0])
            df[0]["StartTime"]   = df[0]["StartTime"]
            df[0]["EndTime"]     = df[i]["EndTime"] 
            

        start = blmtime.strForFilenameFromTime( blmtime.timeFromStr(  df[0]["StartTime"][0] ))
        end   = blmtime.strForFilenameFromTime( blmtime.timeFromStr( df[0]["EndTime"][0]))
        filename = self.output_dir_path+'/BlmDose_t1_%s_t2_%s_fill_%d_bm_%s_RS%02d.csv' %( start, end, fill, mode , rs )
        print("OUTPUT FILE: ", filename)
        df[0].to_csv(filename, index=False)
        [ os.remove(f) for f in selectedList ]
        return

        

    def _filterDataBeamPresent(self, tt_data, vv_data, blmList):
        sample_size = len(tt_data)
    
        # reduce data sample
        self.data_beam_present = {}
        self.data_beam_present["time"] = []
        
        # Here the variables
        for var in vv_data.keys():
            self.data_beam_present[var] = []
        
        # Only keep the data when beam 1 and beam 2 are present and their energies are around 6.5 TeV.
        self.data_beam_present["test"] = []
        for i in range(sample_size):
            if vv_data[self.beamPresentB1][i] == 1 and vv_data[self.beamPresentB2][i] == 1 :
                if self._isGoodenergy(vv_data[self.beamEnergyB1][i], vv_data[self.beamEnergyB2][i]):
                    self.data_beam_present["time"] += [  tt_data[i] ]
                    self.data_beam_present["test"] += [0.5]
                    for var in vv_data.keys():
                        self.data_beam_present[var] += [ vv_data[var][i] ] 
        return 

    def _isGoodenergy(self,energy1,energy2):
        if energy1<self.energy: return False
        if energy2<self.energy: return False
        return True
   
    def _sumData(self):
        self.sumlumiAlice = sum(self.data_beam_present[self.lumiAlice])
        self.sumlumiAtlas = sum(self.data_beam_present[self.lumiAtlas])
        self.sumlumiCMS = sum(self.data_beam_present[self.lumiCMS])
        self.sumlumiLHCb = sum(self.data_beam_present[self.lumiLHCb])
        self.totIntB1 = max(self.data_beam_present[self.beamIntensityB1])
        self.totIntB2 = max(self.data_beam_present[self.beamIntensityB2])
        self._sumMinusBkg()
        
    def _getBlmNoiseFile( self, t1 ):
        # List of files with noise analysis
        listInputNoiseFiles = glob.glob(self.noise_dir_path+"/BlmNoise_*.csv")
        listInputNoiseFiles.sort()
        if len(listInputNoiseFiles) == 0:
            # Cambio aqui el self.output_dir_path por self.noise_dir_path
            print("No files in directory: ", self.noise_dir_path)
            return
        
        # Compare t1 with ti_file in order to select files
        t1_sec = blmtime.timeFromStr(t1)
        selected_file = listInputNoiseFiles[0]
        for f in listInputNoiseFiles:
            #print(f)
            t1_file = (f.split(".csv")[0]).split("BlmNoise_")[1]
            t1_file = t1_file.split("_fill")[0]
            t1_file_sec =  blmtime.timeFromFilenameFromStr(t1_file) 
            if t1_file_sec > t1_sec: return selected_file
            selected_file = f
        return
		
    def _readBkg( self, t1 ):
        ## Update Monitor by BlmMonitor
        selected_filename = self._getBlmNoiseFile( t1 )    
        if selected_filename == None:
            print("BlmNoise file does not exists")
            return
        print("BlmNoise file: ", selected_filename)
        df=pandas.read_csv(selected_filename,encoding='utf-7')
        self.bkg={}
        for i in range(len(df["BlmMonitor"])):
            self.bkg[df["BlmMonitor"][i]] = df["BlmOffset"][i]
        return self.bkg

    
    def _sumMinusBkg(self):
        # Substract the bkg and sum over all the data for each blm
        self.dataDose={}
        for var in self.data_beam_present.keys():
            if "BLM" in var:
                blm = var.split(":")[0]
                s12 = [ s1-self.bkg[blm] for s1 in self.data_beam_present[var] ]
                self.dataDose[blm] = np.sum( s12 ) 
        return
     
    def _writeFile(self, start, end, filename = "BlmLumiStudy.csv"):
        starttime = start
        endtime   = end
        if not os.path.exists(self.output_dir_path): os.makedirs(self.output_dir_path)
        self.outputFilename = filename
        print("Writing: ",self.outputFilename)
        with open( self.outputFilename, "w" ) as csvfile:
            fieldNames = ["FillNumber","StartTime","EndTime", "BeamMode", "TotLumiAlice","TotLumiAtlas","TotLumiCMS","TotLumiLHCb","TotIntB1","TotIntB2", "BlmMonitor", "BlmDcum", "RS", "BlmDose"]
            writer = csv.DictWriter( csvfile, fieldnames = fieldNames )
            writer.writeheader()        
            for blm in self.dataDose.keys():
                listforFile = {'FillNumber' : self.current_fill, 'StartTime': starttime, 'EndTime' : endtime, 'BeamMode' : self.currentBeamMode, 'TotLumiAlice' : self.sumlumiAlice, 'TotLumiAtlas' : self.sumlumiAtlas , 'TotLumiCMS' : self.sumlumiCMS , 'TotLumiLHCb' : self.sumlumiLHCb, 'TotIntB1' : self.totIntB1, 'TotIntB2' : self.totIntB2 , 'RS' : "RS%02d" %(self.RS)}                
                listforFile["BlmDcum"] = self._getDcum(blm)
                listforFile["BlmMonitor"] = blm
                listforFile["BlmDose"] = self.dataDose[blm]
                writer.writerow( listforFile )
        return
  

    def _getBlmDoseFileList( self, t1, t2):
        # List of files with noise analysis
        listInputNoiseFiles = glob.glob(self.output_dir_path+"/BlmDose_*.csv")
        listInputNoiseFiles.sort()

        if len(listInputNoiseFiles) == 0:
            print("No files in directory: ", self.output_dir_path)
            return
        
        # Search files between dates
        # Compare t1 with ti_file in order to select files
        t1_sec = blmtime.timeFromStr(t1)
        t2_sec = blmtime.timeFromStr(t2)
        selectedList=[]
        dates = []
        for f in listInputNoiseFiles:
            #print(f)
            t1_file = (f.split(".csv")[0]).split("t1_")[1]
            t1_file = t1_file.split("_t2")[0]
            t1_file_sec =  blmtime.timeFromFilenameFromStr(t1_file) 
            #print(t1_file)
            if t1_file_sec < t1_sec: continue
            if t1_file_sec > t2_sec: break
            selectedList.append( f )
            dates += [t1_file_sec]
        return selectedList, dates


    def _getBlmDoseFileList_TFG( self, t1, t2):
        # List of files with noise analysis
        selectedList=[]
        dates = []
        for i in [5,6,7,8]:
            listInputNoiseFiles = glob.glob(self.output_dir_path+"%s"%(i)+"/BlmDose_*.csv")
            print(len(listInputNoiseFiles))
            listInputNoiseFiles.sort()

            if len(listInputNoiseFiles) == 0:
                print("No files in directory: ", self.output_dir_path)
                return
        
            # Search files between dates
            # Compare t1 with ti_file in order to select files
            t1_sec = blmtime.timeFromStr(t1)
            t2_sec = blmtime.timeFromStr(t2)
            
            for f in listInputNoiseFiles:
                #print(f)
                t1_file = (f.split(".csv")[0]).split("t1_")[1]
                t1_file = t1_file.split("_t2")[0]
                t1_file_sec =  blmtime.timeFromFilenameFromStr(t1_file) 
                #print(t1_file)
                if t1_file_sec < t1_sec: continue
                if t1_file_sec > t2_sec: break
                selectedList.append( f )
                dates += [t1_file_sec]
        return selectedList, dates



    def plotDoseVsTime(self, t1, t2, blmList = [],  progressBar = []):
        print("** Plot BLM dose vs time **")
        try:
            progressBar.setValue(0)

        except AttributeError:
            pass

        selectedList, dates = self._getBlmDoseFileList( t1, t2 )
           
        nSelected = len(selectedList)
        if nSelected == 0: 
            print("No files selected")
            return
        print("Number of files read:",nSelected)


        try:
            progressBar.setMaximum(nSelected+2)
            progressBar.setValue(1)

        except AttributeError:
            pass



        # Create Pandas DataFrame of concat files
        df = pandas.concat(map(pandas.read_csv, selectedList ))

        ### TENGO QUE ACUMULAR LA DOSE, no SOLO PLOT !

        accumulatedDose = {}
        for filename in selectedList:
            df = pandas.read_csv( filename )
            for blm in blmList:
                #print(df[df["BlmMonitor"] == blm]["BlmDose"].values[0])
                #sys.exit()
                try:
                    accumulatedDose[blm] += [accumulatedDose[blm][-1] + df[ df["BlmMonitor"] == blm]["BlmDose"].values[0]]
                except KeyError:
                    accumulatedDose[blm] =  [df[ df["BlmMonitor"] == blm]["BlmDose"].values[0]]


            try:
                progressBar.setValue(progressBar.value() + 1 )

            except AttributeError:
                pass


        #print(accumulatedDose)
        #sys.exit()
        #self.fig_size = (10,6)
        self.fig_counter += 1
        self.fig_size = (10,4)
        plt.figure(self.fig_counter, figsize = self.fig_size)
        for blm in blmList:
            plt.title("From %s to %s for RS%02d" %(t1.split()[0], t2.split()[0], self.RS ))
            plt.errorbar( dates, accumulatedDose[blm],capsize=4,fmt="o-",label=blm)
            plt.ylabel('Cumulative BLM dose [Gy]')
        plt.legend()
        pytimber.set_xaxis_date()
        #plt.yscale('log')
        plt.ticklabel_format(axis='y', style='sci', scilimits=(-3,3))
        plt.tight_layout()
        try:
            progressBar.setValue(progressBar.value() + 1 )

        except AttributeError:
            pass

        if self.ShowPlots == True: plt.show()
        
        return

    def checkIR(self, text, keyPlot):
        if keyPlot == "Int":
            if "B1" in text:
                keyPlot2 = "TotIntB1"
            if "B2" in text:
                keyPlot2 = "TotIntB2"

        if keyPlot == "Lumi":
            if "R1" in text or "L1" in text:
                keyPlot2 = "TotLumiAtlas"

            if "L2" in text or "R2" in text or "L3" in text:
                keyPlot2 = "TotLumiAlice"

            if "R3" in text or "L4" in text or "R4" in text or "L5" in text or "R5" in text or "L6" in text or "R6" in text or "L7" in text:
                keyPlot2 = "TotLumiCMS"

            if "R7" in text or "L8" in text or "R8" in text:
                keyPlot2 = "TotLumiLHCb"


        return keyPlot2

    def plotDoseVsKeys(self, t1, t2, keyPlot = "Time", keyCumulate = True, blmList = [],progressBar=[]):
        print("** Plot BLM dose vs time **")
        try:
            progressBar.setValue(0)

        except AttributeError:
            pass

        #selectedList, dates = self._getBlmDoseFileList( t1, t2 )
        selectedList, dates = self._getBlmDoseFileList_TFG( t1, t2 )
           
        nSelected = len(selectedList)
        if nSelected == 0: 
            print("No files selected")
            return
        print("Number of files read:",nSelected)

        # Create Pandas DataFrame of concat files
        # Esto para que era??
        #df = pandas.concat(map(pandas.read_csv, selectedList ))


        try:
            progressBar.setMaximum(nSelected+2)
            progressBar.setValue(1)

        except AttributeError:
            pass

        accumulatedDose = {}
        accumulatedLumi = {}
        accumulatedInt = {}
        keyPlot2 = {}
    
        
        for filename in selectedList:
            df = pandas.read_csv( filename )
            timestart = blmtime.timeFromStr(df["StartTime"][0])
            if df["StartTime"][0] == "2015-12-13 21:31:34":
                #print(accumulatedLumi["BLMTI.04L5.B2E10_TANC.4L5"][-1])
                print(accumulatedLumi["BLMTI.04L1.B2E10_TANAL.4L1"][-1])
                #print(accumulatedInt["BLMTI.06L7.B2I10_TCLA.A6L7.B2"][-1])
            if df["StartTime"][0] == "2016-12-04 02:00:31":
                #print(accumulatedLumi["BLMTI.04L5.B2E10_TANC.4L5"][-1])
                print(accumulatedLumi["BLMTI.04L1.B2E10_TANAL.4L1"][-1])
                #print(accumulatedInt["BLMTI.06L7.B2I10_TCLA.A6L7.B2"][-1])
            if df["StartTime"][0] == "2017-12-04 03:56:54":
                #print(accumulatedLumi["BLMTI.04L5.B2E10_TANC.4L5"][-1])
                print(accumulatedLumi["BLMTI.04L1.B2E10_TANAL.4L1"][-1])
                #print(accumulatedInt["BLMTI.06L7.B2I10_TCLA.A6L7.B2"][-1])
            timend = blmtime.timeFromStr(df["EndTime"][0])
            interval = timend-timestart
            

            for blm in blmList:                
                try:
                    if keyCumulate:
                        accumulatedDose[blm] += [accumulatedDose[blm][-1] + df[ df["BlmMonitor"] == blm]["BlmDose"].values[0]]
                    else:
                        accumulatedDose[blm] += [df[ df["BlmMonitor"] == blm]["BlmDose"].values[0]]
                except KeyError:
                    accumulatedDose[blm] =  [df[ df["BlmMonitor"] == blm]["BlmDose"].values[0]]
               

                if keyPlot == "Lumi":
                    IR = blm.split(".")[1]
                    keyPlot2[blm] = self.checkIR(IR, keyPlot)
                    try:
                        if keyCumulate:
                            accumulatedLumi[blm] += [accumulatedLumi[blm][-1] + df[keyPlot2[blm]].values[0]]
                        else:
                            accumulatedLumi[blm] += [df[keyPlot2[blm]].values[0]]
                    except KeyError:
                        accumulatedLumi[blm] =  [df[keyPlot2[blm]].values[0]]
                
                if keyPlot == "Int":
                    beam = blm.split(".")[2]
                    keyPlot2[blm] = self.checkIR(beam, keyPlot)
                    try:
                        if keyCumulate:
                            accumulatedInt[blm] += [accumulatedInt[blm][-1] + df[keyPlot2[blm]].values[0]*interval]
                        else:
                            accumulatedInt[blm] += [df[keyPlot2[blm]].values[0]*interval]
                            #accumulatedInt[blm] += [df[keyPlot2[blm]].values[0]]
                            
                    except KeyError:
                        accumulatedInt[blm] =  [df[keyPlot2[blm]].values[0]*interval]
                        #accumulatedInt[blm] =  [df[keyPlot2[blm]].values[0]]
                        

            try:
                progressBar.setValue(progressBar.value() + 1 )

            except AttributeError:
                pass

        
        #print(accumulatedDose)
        #sys.exit()
        #self.fig_size = (10,6)
        self.fig_counter += 1
        self.fig_size = (10,4)
        font = {'family' : 'DejaVu Sans','weight' : 'normal','size'   : 20}
        matplotlib.rc('font', **font)
        plt.figure(self.fig_counter, figsize = self.fig_size)
        for blm in blmList:
            #plt.title("From %s to %s for RS%02d" %(t1.split()[0], t2.split()[0], self.RS ))
            if keyPlot == "Time":
                plt.errorbar( dates, accumulatedDose[blm],capsize=4,fmt="o",label=blm)
                pytimber.set_xaxis_date()
            if keyPlot == "Lumi":
                #plt.errorbar( np.multiply(accumulatedLumi[blm], 1e-9), accumulatedDose[blm],capsize=4,fmt="o")
                #x = np.array( np.multiply(accumulatedLumi[blm], 1e-9)).reshape((-1,1))
                #y = np.array(accumulatedDose[blm])
                #model = LinearRegression().fit(x,y)
                #r_sq = model.score(x,y)
                #print(r_sq)
                #y_pred = model.predict(x)
                #slope = model.coef_[0]
                #print(slope, r_sq, model.intercept_)
                print(accumulatedDose[blm][-1])
                print(accumulatedLumi[blm][-1])
                #plt.errorbar( x, y_pred, fmt = "-", label="{}: y={:.1f}x + {:.1f}; R$^2$:{:.4f}".format(blm,slope, model.intercept_,r_sq))
                #plt.errorbar( x, y_pred, fmt = "-", label="y={:.1f}x + {:.1f}; R$^2$:{:.4f}".format(slope, model.intercept_,r_sq))
                #plt.vlines(np.multiply(4201519347.4202995,1e-9), 0, accumulatedDose[blm][-1], colors = 'k')
                #plt.vlines(np.multiply(44779588839.487946,1e-9), 0, accumulatedDose[blm][-1], colors = 'k')
                #plt.vlines(np.multiply(96331609068.40001,1e-9), 0, accumulatedDose[blm][-1], colors = 'k')
                plt.vlines(np.multiply(4671977214.467811,1e-9), 0, accumulatedDose[blm][-1], colors = 'k')
                plt.vlines(np.multiply(44748837106.40662,1e-9), 0, accumulatedDose[blm][-1], colors = 'k')
                plt.vlines(np.multiply(96558439249.10675,1e-9), 0, accumulatedDose[blm][-1], colors = 'k')
                if blm == "BLMTI.04L5.B2E10_TANC.4L5":
                    plt.plot(np.multiply(accumulatedLumi[blm],1e-9), accumulatedDose[blm], color = 'red', marker = "o")
                    #plt.plot(x_1, y_pred_1, color = 'green')
                    #plt.plot(x_2, y_pred_2, color = 'green')
                    plt.plot(x, y_pred, color = 'green')
                if blm == "BLMTI.04R5.B1E10_TANC.4R5":
                    plt.plot(np.multiply(accumulatedLumi[blm],1e-9), accumulatedDose[blm], color = 'blue', marker = "o")
                    #plt.plot(x_1, y_pred_1, color ='orange')
                    #plt.plot(x_2, y_pred_2, color = 'orange')
                    plt.plot(x, y_pred, color = 'orange')


                if blm == "BLMTI.04L1.B2E10_TANAL.4L1":
                    plt.plot(np.multiply(accumulatedLumi[blm],1e-9), accumulatedDose[blm], color = 'red', marker = "o")
                    x_1 = np.array( np.multiply(accumulatedLumi[blm][0:1077],1e-9)).reshape((-1,1))
                    y_1 = np.array(accumulatedDose[blm][0:1077])
                    model_1 = LinearRegression().fit(x_1, y_1)
                    r_sq_1= model_1.score(x_1,y_1)
                    y_pred_1 = model_1.predict(x_1)
                    slope_1 = model_1.coef_[0]
                    print(slope_1, r_sq_1, model_1.intercept_)

                    x_2 = np.array( np.multiply(accumulatedLumi[blm][1077:],1e-9)).reshape((-1,1))
                    y_2 = np.array( accumulatedDose[blm][1077:])
                    model_2 = LinearRegression().fit(x_2, y_2)
                    r_sq_2= model_2.score(x_2,y_2)
                    y_pred_2 = model_2.predict(x_2)
                    slope_2 = model_2.coef_[0]
                    print(slope_2, r_sq_2, model_2.intercept_)


                    plt.plot(x_1, y_pred_1, color = 'green')
                    plt.plot(x_2, y_pred_2, color = 'green')

                if blm == "BLMTI.04R1.B1E10_TANAR.4R1":
                    plt.plot(np.multiply(accumulatedLumi[blm],1e-9), accumulatedDose[blm], color = 'blue', marker = "o")
                    x_1 = np.array( np.multiply(accumulatedLumi[blm][0:2320], 1e-9)).reshape((-1,1))
                    y_1 = np.array( accumulatedDose[blm][0:2320])
                    model_1 = LinearRegression().fit(x_1, y_1)
                    r_sq_1= model_1.score(x_1,y_1)
                    y_pred_1 = model_1.predict(x_1)
                    slope_1 = model_1.coef_[0]
                    print(slope_1, r_sq_1, model_1.intercept_)

                    x_2 = np.array( np.multiply(accumulatedLumi[blm][2320:],1e-9)).reshape((-1,1))
                    y_2 = np.array( accumulatedDose[blm][2320:])
                    model_2 = LinearRegression().fit(x_2, y_2)
                    r_sq_2= model_2.score(x_2,y_2)
                    y_pred_2 = model_2.predict(x_2)
                    slope_2 = model_2.coef_[0]
                    print(slope_2, r_sq_2, model_2.intercept_)


                    plt.plot(x_1, y_pred_1, color = 'orange')
                    plt.plot(x_2, y_pred_2, color = 'orange')

                if keyCumulate:
                    plt.xlabel('Integrated ATLAS Luminosity [fb$  ^{-1}$]')
                else:
                    plt.xlabel('Integrated Luminosity in a period [fb$  ^{-1}$]')
            if keyPlot == "Int":
                #plt.errorbar( accumulatedInt[blm], accumulatedDose[blm],capsize=4,fmt="o",label=blm)
                
                x_1 = np.array( accumulatedInt[blm][0:3610]).reshape((-1,1))
                y_1 = np.array(accumulatedDose[blm][0:3610])
                model_1 = LinearRegression().fit(x_1, y_1)
                r_sq_1= model_1.score(x_1,y_1)
                y_pred_1 = model_1.predict(x_1)
                slope_1 = model_1.coef_[0]
                print(slope_1, r_sq_1, model_1.intercept_)

                x_2 = np.array( accumulatedInt[blm][3610:]).reshape((-1,1))
                y_2 = np.array(accumulatedDose[blm][3610:])
                model_2 = LinearRegression().fit(x_2, y_2)
                r_sq_2= model_2.score(x_2,y_2)
                y_pred_2 = model_2.predict(x_2)
                slope_2 = model_2.coef_[0]
                print(slope_2, r_sq_2, model_2.intercept_)
                #print(accumulatedDose[blm][-1])
                #plt.errorbar( x_1, y_pred_1, fmt = "-", label="{}: y={:.1f}x + {:.1f}; R$^2$:{:.4f}".format(blm,slope_1, model_1.intercept_,r_sq_1))
                #plt.errorbar( x_2, y_pred_2, fmt = "-", label="y={:.1f}x + {:.1f}; R$^2$:{:.4f}".format(slope_2, model_2.intercept_,r_sq_2))
                plt.vlines(3.1845711895874195e20, 0, accumulatedDose[blm][-1], colors = 'k')
                plt.vlines(1.839576103331324e21, 0, accumulatedDose[blm][-1], colors = 'k')
                plt.vlines(3.487941717492687e21, 0, accumulatedDose[blm][-1], colors = 'k')
                if blm == "BLMTI.06L7.B2I10_TCLA.A6L7.B2":
                    plt.plot(accumulatedInt[blm], accumulatedDose[blm], color = 'red', marker = "o")
                    plt.plot(x_1, y_pred_1, color = 'green')
                    plt.plot(x_2, y_pred_2, color = 'green')
                if blm == "BLMEI.06L7.B1E10_TCSM.A6L7.B1":
                    plt.plot(accumulatedInt[blm], accumulatedDose[blm], color = 'blue', marker = "o")
                    plt.plot(x_1, y_pred_1, color ='orange')
                    plt.plot(x_2, y_pred_2, color = 'orange')
                
                if keyCumulate:
                    plt.xlabel('Integrated maximum intensity multiplied by the time period [charges$\\cdot$s]')
                else:
                    plt.xlabel('Maximum intensity in a period [charges]')
            

        if keyCumulate:
            plt.ylabel('Integrated BLM dose [Gy]')

        else:
            plt.ylabel('Integrated BLM dose in a period [Gy]')
        #plt.legend()
        
        #plt.yscale('log')
        plt.ticklabel_format(axis='y', style='sci', scilimits=(-3,3))
        plt.tight_layout()
        try:
            progressBar.setValue(progressBar.value() + 1 )

        except AttributeError:
            pass
        if self.ShowPlots == True: plt.show()
        
        return

    def plotDoseAlongRing( self, t1, t2, blmList = [],progressBar=[] ):
        print("** Plot BLM dose vs Position **")
        try:
            progressBar.setValue(0)

        except AttributeError:
            pass
        # if blmList is empty then use all Blm in the default file
        # if blmList is not empty use the BLMs in the list
        selectedList, dates = self._getBlmDoseFileList_TFG( t1, t2 )
        #selectedList, dates = self._getBlmDoseFileList( t1, t2 )
        #selectedList = selectedList[0:1]

        nSelected = len(selectedList)
        if nSelected == 0: 
            print("ERROR: No files selected")
            return
        print("Number of files read:",nSelected)

        
        

        fileBlmDcum = "lsa/20200213_BlmDbLsa.csv"

        df_blm = pandas.read_csv(fileBlmDcum)
        df_blm = df_blm[0:3938]
        blmNames = df_blm["MONITOR_EXP_NAME"]
        df_blm["DCUM"] = df_blm["DCUM"]/100

        try:
            progressBar.setMaximum(nSelected*len(blmNames)+2)
            progressBar.setValue(1)

        except AttributeError:
            pass

        
        totaldose = {}


        # Create Pandas DataFrame for TOTAL DOSE
        for filename in selectedList:
            #print("Reading 1: ", filename)
            df = pandas.read_csv( filename )

            for blm in blmNames:
                try:
                    totaldose[blm] += df[df["BlmMonitor"] == blm]["BlmDose"].values[0]
                except KeyError:
                    try:
                        totaldose[blm] = df[df["BlmMonitor"] == blm]["BlmDose"].values[0]
                    except IndexError:
                
                        totaldose[blm] = 0

                except IndexError:
                    #print(blm)
                    pass
                        

                try:
                    progressBar.setValue(progressBar.value() + 1 )

                except AttributeError:
                    pass




        #print(totaldose)
        #df["TotalBlmDose"] = dose

        
        #newdf = df.sort_values("TotalBlmDose", ascending=False)

        #print(totaldose.items())

        totaldosesorted = sorted(totaldose.items(), key=operator.itemgetter(1),reverse=True)

        #print(totaldosesorted[0:10])

        

        print(" ***** BLM Names with higher accumulated dose ***** ")
        #highestDoseBLM = newdf["BlmMonitor"][0:10]
        highestDoseBLM = totaldosesorted[0:10]
        #lowestDoseBLM = newdf["BlmMonitor"][len(newdf["BlmMonitor"])-11:len(newdf["BlmMonitor"])-1]
        lowestDoseBLM = totaldosesorted[len(totaldosesorted)-10:len(totaldosesorted)]
        for blm in highestDoseBLM:

            #print( "%30s" %(blm),"->","Dcum:","%.2f" %(df[df["BlmMonitor"] == blm]["BlmDcum"].values[0]),"->", "Accumulated dose:", "%g" %(df[df["BlmMonitor"] == blm]["TotalBlmDose"].values[0]))
            print( "%30s" %(blm[0]),"->","Dcum:","%.2f" %(df_blm[df_blm["MONITOR_EXP_NAME"] == blm[0]]["DCUM"].values[0]),"->", "Accumulated dose:", "%g" %(blm[1]))
           
        print(" ************************************* ")

        for blm in lowestDoseBLM:

            #print( "%30s" %(blm),"->","Dcum:","%.2f" %(df[df["BlmMonitor"] == blm]["BlmDcum"].values[0]),"->", "Accumulated dose:", "%g" %(df[df["BlmMonitor"] == blm]["TotalBlmDose"].values[0]))
            print( "%30s" %(blm[0]),"->","Dcum:","%.2f" %(df_blm[df_blm["MONITOR_EXP_NAME"] == blm[0]]["DCUM"].values[0]),"->", "Accumulated dose:", "%g" %(blm[1]))

        print(" ************************************* ")
        
        #sys.exit()
        # Reduce the DataFrame including only selected blm in the blmList
        #if len(blmList) != 0 : df = df[ [ i in blmList for i in df["BlmMonitor"] ] ]
        if len(blmList) != 0 : df = df[ [ i in blmList for i in blmNames] ]
        
        #df_ic = df[ [ self._getBlmType(blm)  == "IC" for blm in df["BlmMonitor"] ] ]
        #df_lic = df[ [ self._getBlmType(blm) == "LIC" for blm in df["BlmMonitor"] ] ]
        #df_sem = df[ [ self._getBlmType(blm) == "SEM" for blm in df["BlmMonitor"] ] ]
        #df_dump_ic = df[ [ self._getBlmType(blm) == "DUMP_IC" for blm in df["BlmMonitor"] ] ]
        #df_dump_lic = df[ [ self._getBlmType(blm) == "DUMP_LIC" for blm in df["BlmMonitor"] ] ]
        #df_dump_sem = df[ [ self._getBlmType(blm) == "DUMP_SEM" for blm in df["BlmMonitor"] ] ]

        
        df_ic = df_blm[ [ self._getBlmType(blm)  == "IC" for blm in blmNames ] ]
        df_lic = df_blm[ [ self._getBlmType(blm) == "LIC" for blm in blmNames ] ]
        df_sem = df_blm[ [ self._getBlmType(blm) == "SEM" for blm in  blmNames] ]
        df_dump_ic = df_blm[ [ self._getBlmType(blm) == "DUMP_IC" for blm in blmNames ] ]
        df_dump_lic = df_blm[ [ self._getBlmType(blm) == "DUMP_LIC" for blm in blmNames ] ]
        df_dump_sem = df_blm[ [ self._getBlmType(blm) == "DUMP_SEM" for blm in blmNames ] ]


        #print(df_dump_sem)
        #print(df_dump_sem["MONITOR_EXP_NAME"])
        #sys.exit()


        #df_ic["TotalBlmDose"] = []
        

        

        """
        for blm in df_ic["MONITOR_EXP_NAME"]:
            df_ic["TotalBlmDose"] += [totaldose[blm]]

        for blm in df_lic["MONITOR_EXP_NAME"]:
            df_lic["TotalBlmDose"] += [totaldose[blm]]

        for blm in df_sem["MONITOR_EXP_NAME"]:
            df_sem["TotalBlmDose"] += [totaldose[blm]]


        for blm in df_dump_ic["MONITOR_EXP_NAME"]:
            df_dump_ic["TotalBlmDose"] += [totaldose[blm]]


        for blm in df_dump_lic["MONITOR_EXP_NAME"]:
            df_dump_lic["TotalBlmDose"] += [totaldose[blm]]


        for blm in df_dump_sem["MONITOR_EXP_NAME"]:
            df_dump_sem["TotalBlmDose"] += [totaldose[blm]]
        """
        
        pandas.set_option('mode.chained_assignment', None)


        
        
        df_ic["TotalBlmDose"] = [totaldose[blm] for blm in df_ic.loc[:,"MONITOR_EXP_NAME"]]
        df_lic["TotalBlmDose"] = [totaldose[blm] for blm in df_lic.loc[:,"MONITOR_EXP_NAME"]]
        df_sem["TotalBlmDose"] = [totaldose[blm] for blm in df_sem.loc[:,"MONITOR_EXP_NAME"]]
        df_dump_ic["TotalBlmDose"] = [totaldose[blm] for blm in df_dump_ic.loc[:,"MONITOR_EXP_NAME"]]
        df_dump_lic["TotalBlmDose"] = [totaldose[blm] for blm in df_dump_lic.loc[:,"MONITOR_EXP_NAME"]]
        df_dump_sem["TotalBlmDose"] = [totaldose[blm] for blm in df_dump_sem.loc[:,"MONITOR_EXP_NAME"]]



        pandas.set_option('mode.chained_assignment', 'raise')

        #print(df_dump_sem["TotalBlmDose"])
        #print(df_dump_lic["BlmMonitor"])
        #print(df_dump_sem["BlmMonitor"])
        
        
    
        sortedic = df_ic.sort_values("TotalBlmDose", ascending=False)
        sortedlic = df_lic.sort_values("TotalBlmDose", ascending=False)
        sortedsem = df_sem.sort_values("TotalBlmDose", ascending=False)
        sorteddumpic = df_dump_ic.sort_values("TotalBlmDose", ascending=False)
        sorteddumplic = df_dump_lic.sort_values("TotalBlmDose", ascending=False)
        sorteddumpsem = df_dump_sem.sort_values("TotalBlmDose", ascending=False)


        #print(totaldosesorted[0:10])

        

        print(" ***** BLM Names with higher accumulated dose per type***** ")
        #highestDoseBLM = newdf["BlmMonitor"][0:10]
        highestDoseBLMic = sortedic["MONITOR_EXP_NAME"][0:10]
        #lowestDoseBLM = newdf["BlmMonitor"][len(newdf["BlmMonitor"])-11:len(newdf["BlmMonitor"])-1]
        #lowestDoseBLM = totaldosesorted[len(totaldosesorted)-10:len(totaldosesorted)]
        for blm in highestDoseBLMic:

            #print( "%30s" %(blm),"->","Dcum:","%.2f" %(df[df["BlmMonitor"] == blm]["BlmDcum"].values[0]),"->", "Accumulated dose:", "%g" %(df[df["BlmMonitor"] == blm]["TotalBlmDose"].values[0]))
            print( "%30s" %(blm),"->","Dcum:","%.2f" %(sortedic[sortedic["MONITOR_EXP_NAME"] == blm]["DCUM"].values[0]),"->", "Accumulated dose:", "%g" %(sortedic[sortedic["MONITOR_EXP_NAME"] == blm]["TotalBlmDose"].values[0]))
           
        print(" ************************************* ")


        print(" ***** BLM Names with higher accumulated dose per type***** ")
        #highestDoseBLM = newdf["BlmMonitor"][0:10]
        highestDoseBLMlic = sortedlic["MONITOR_EXP_NAME"][0:10]
        #lowestDoseBLM = newdf["BlmMonitor"][len(newdf["BlmMonitor"])-11:len(newdf["BlmMonitor"])-1]
        #lowestDoseBLM = totaldosesorted[len(totaldosesorted)-10:len(totaldosesorted)]
        for blm in highestDoseBLMlic:

            #print( "%30s" %(blm),"->","Dcum:","%.2f" %(df[df["BlmMonitor"] == blm]["BlmDcum"].values[0]),"->", "Accumulated dose:", "%g" %(df[df["BlmMonitor"] == blm]["TotalBlmDose"].values[0]))
            print( "%30s" %(blm),"->","Dcum:","%.2f" %(sortedlic[sortedlic["MONITOR_EXP_NAME"] == blm]["DCUM"].values[0]),"->", "Accumulated dose:", "%g" %(sortedlic[sortedlic["MONITOR_EXP_NAME"] == blm]["TotalBlmDose"].values[0]))
           
        print(" ************************************* ")



        print(" ***** BLM Names with higher accumulated dose per type***** ")
        #highestDoseBLM = newdf["BlmMonitor"][0:10]
        highestDoseBLMsem = sortedsem["MONITOR_EXP_NAME"][0:10]
        #lowestDoseBLM = newdf["BlmMonitor"][len(newdf["BlmMonitor"])-11:len(newdf["BlmMonitor"])-1]
        #lowestDoseBLM = totaldosesorted[len(totaldosesorted)-10:len(totaldosesorted)]
        for blm in highestDoseBLMsem:

            #print( "%30s" %(blm),"->","Dcum:","%.2f" %(df[df["BlmMonitor"] == blm]["BlmDcum"].values[0]),"->", "Accumulated dose:", "%g" %(df[df["BlmMonitor"] == blm]["TotalBlmDose"].values[0]))
            print( "%30s" %(blm),"->","Dcum:","%.2f" %(sortedsem[sortedsem["MONITOR_EXP_NAME"] == blm]["DCUM"].values[0]),"->", "Accumulated dose:", "%g" %(sortedsem[sortedsem["MONITOR_EXP_NAME"] == blm]["TotalBlmDose"].values[0]))
           
        print(" ************************************* ")


       
        print(" ***** BLM Names with higher accumulated dose per type***** ")
        #highestDoseBLM = newdf["BlmMonitor"][0:10]
        highestDoseBLMdump_ic = sorteddumpic["MONITOR_EXP_NAME"][0:10]
        #lowestDoseBLM = newdf["BlmMonitor"][len(newdf["BlmMonitor"])-11:len(newdf["BlmMonitor"])-1]
        #lowestDoseBLM = totaldosesorted[len(totaldosesorted)-10:len(totaldosesorted)]
        for blm in highestDoseBLMdump_ic:

            #print( "%30s" %(blm),"->","Dcum:","%.2f" %(df[df["BlmMonitor"] == blm]["BlmDcum"].values[0]),"->", "Accumulated dose:", "%g" %(df[df["BlmMonitor"] == blm]["TotalBlmDose"].values[0]))
            print( "%30s" %(blm),"->","Dcum:","%.2f" %(sorteddumpic[sorteddumpic["MONITOR_EXP_NAME"] == blm]["DCUM"].values[0]),"->", "Accumulated dose:", "%g" %(sorteddumpic[sorteddumpic["MONITOR_EXP_NAME"] == blm]["TotalBlmDose"].values[0]))
           
        print(" ************************************* ")



        print(" ***** BLM Names with higher accumulated dose per type***** ")
        #highestDoseBLM = newdf["BlmMonitor"][0:10]
        highestDoseBLMdump_lic = sorteddumplic["MONITOR_EXP_NAME"]
        #lowestDoseBLM = newdf["BlmMonitor"][len(newdf["BlmMonitor"])-11:len(newdf["BlmMonitor"])-1]
        #lowestDoseBLM = totaldosesorted[len(totaldosesorted)-10:len(totaldosesorted)]
        for blm in highestDoseBLMdump_lic:

            #print( "%30s" %(blm),"->","Dcum:","%.2f" %(df[df["BlmMonitor"] == blm]["BlmDcum"].values[0]),"->", "Accumulated dose:", "%g" %(df[df["BlmMonitor"] == blm]["TotalBlmDose"].values[0]))
            print( "%30s" %(blm),"->","Dcum:","%.2f" %(sorteddumplic[sorteddumplic["MONITOR_EXP_NAME"] == blm]["DCUM"].values[0]),"->", "Accumulated dose:", "%g" %(sorteddumplic[sorteddumplic["MONITOR_EXP_NAME"] == blm]["TotalBlmDose"].values[0]))
           
        print(" ************************************* ")




        print(" ***** BLM Names with higher accumulated dose per type***** ")
        #highestDoseBLM = newdf["BlmMonitor"][0:10]
        highestDoseBLMdump_sem = sorteddumpsem["MONITOR_EXP_NAME"]
        #lowestDoseBLM = newdf["BlmMonitor"][len(newdf["BlmMonitor"])-11:len(newdf["BlmMonitor"])-1]
        #lowestDoseBLM = totaldosesorted[len(totaldosesorted)-10:len(totaldosesorted)]
        for blm in highestDoseBLMdump_sem:

            #print( "%30s" %(blm),"->","Dcum:","%.2f" %(df[df["BlmMonitor"] == blm]["BlmDcum"].values[0]),"->", "Accumulated dose:", "%g" %(df[df["BlmMonitor"] == blm]["TotalBlmDose"].values[0]))
            print( "%30s" %(blm),"->","Dcum:","%.2f" %(sorteddumpsem[sorteddumpsem["MONITOR_EXP_NAME"] == blm]["DCUM"].values[0]),"->", "Accumulated dose:", "%g" %(sorteddumpsem[sorteddumpsem["MONITOR_EXP_NAME"] == blm]["TotalBlmDose"].values[0]))
           
        print(" ************************************* ")



        #for blm in lowestDoseBLM:

            #print( "%30s" %(blm),"->","Dcum:","%.2f" %(df[df["BlmMonitor"] == blm]["BlmDcum"].values[0]),"->", "Accumulated dose:", "%g" %(df[df["BlmMonitor"] == blm]["TotalBlmDose"].values[0]))
            #print( "%30s" %(blm[0]),"->","Dcum:","%.2f" %(df_blm[df_blm["MONITOR_EXP_NAME"] == blm[0]]["DCUM"].values[0]),"->", "Accumulated dose:", "%g" %(blm[1]))

        #print(" ************************************* ")

        
        filename = self.output_files+'/BlmHighestDoseIC.csv'
        print("OUTPUT FILE: ", filename)
        sortedic.to_csv(filename, index=False)

        filename = self.output_files+'/BlmHighestDoseLIC.csv'
        print("OUTPUT FILE: ", filename)
        sortedlic.to_csv(filename, index=False)

        filename = self.output_files+'/BlmHighestDoseSEM.csv'
        print("OUTPUT FILE: ", filename)
        sortedsem.to_csv(filename, index=False)

        filename = self.output_files+'/BlmHighestDoseDUMPIC.csv'
        print("OUTPUT FILE: ", filename)
        sorteddumpic.to_csv(filename, index=False)

        filename = self.output_files+'/BlmHighestDoseDUMPLIC.csv'
        print("OUTPUT FILE: ", filename)
        sorteddumplic.to_csv(filename, index=False)

        filename = self.output_files+'/BlmHighestDoseDUMPSEM.csv'
        print("OUTPUT FILE: ", filename)
        sorteddumpsem.to_csv(filename, index=False)
        


        #sys.exit()
        
        # PLOT BLM vs SPOS
        font = {'family' : 'DejaVu Sans','weight' : 'normal','size'   : 20}
        matplotlib.rc('font', **font)
        self.fig_size = (10,6)
        self.fig_counter += 1
        fig, axs = plt.subplots(2,1,sharex=True, figsize = self.fig_size , gridspec_kw={'hspace': 0} , num = self.fig_counter)
        #fig.suptitle("From %s to %s for RS%02d" %(t1.split()[0], t2.split()[0], self.RS ))
        # TOTAL DOSE
        if len(df_ic) != 0:
            #axs[0].errorbar(df_ic["BlmDcum"], df_ic["TotalBlmDose"], capsize = 4, fmt="ob", markersize=2, label="IC") 
            axs[0].errorbar(df_ic["DCUM"], df_ic["TotalBlmDose"], capsize = 4, fmt="ob", markersize=2, label="IC") 
        if len(df_lic) != 0:
            #axs[0].errorbar(df_lic["BlmDcum"], df_lic["TotalBlmDose"],capsize = 4, fmt="og",  markersize=2,label="LIC")
            axs[0].errorbar(df_lic["DCUM"], df_lic["TotalBlmDose"],capsize = 4, fmt="og",  markersize=2,label="LIC")
        #if len(df_dump_ic) != 0:
            #axs[0].errorbar(-(df_dump_ic["BlmDcum"]), df_dump_ic["TotalBlmDose"], capsize = 4, fmt="or",  markersize=2,label="DUMP IC")
        #    axs[0].errorbar(-(df_dump_ic["DCUM"]), df_dump_ic["TotalBlmDose"], capsize = 4, fmt="or",  markersize=2,label="DUMP IC")
        #if len(df_dump_lic) !=0:
            #axs[0].errorbar(-(df_dump_lic["BlmDcum"]), df_dump_lic["TotalBlmDose"], capsize = 4, fmt="ok",  markersize=2,label="DUMP LIC")
        #    axs[0].errorbar(-(df_dump_lic["DCUM"]), df_dump_lic["TotalBlmDose"], capsize = 4, fmt="ok",  markersize=2,label="DUMP LIC")
        if len(df_sem) != 0:
            #axs[1].errorbar(df_sem["BlmDcum"], df_sem["TotalBlmDose"],capsize = 4, fmt="om",  markersize=2,label="SEM")
            axs[1].errorbar(df_sem["DCUM"], df_sem["TotalBlmDose"],capsize = 4, fmt="om",  markersize=2,label="SEM")
        #if len(df_dump_sem) != 0:
            #axs[1].errorbar(-(df_dump_sem["BlmDcum"]), df_dump_sem["TotalBlmDose"],capsize = 4, fmt="oc",  markersize=2,label="DUMP SEM")
        #    axs[1].errorbar(-(df_dump_sem["DCUM"]), df_dump_sem["TotalBlmDose"],capsize = 4, fmt="oc",  markersize=2,label="DUMP SEM")
            
        
        #axs[0].set(ylabel='Cumulative BLM dose [Gy]',xlabel='Position in the LHC [m]')
        #axs[1].set(ylabel='Cumulative BLM dose [Gy]',xlabel='Position in the LHC [m]')
        axs[1].set_xlabel("Position in the LHC [m]")
        fig.text(0.03, 0.5, 'BLM total dose [Gy]', ha='center', va='center', rotation='vertical')
        for i in [0,1]:
            axs[i].set_yscale('log')
            #axs[i,j].grid(axis='y')
            #axs[i].legend()
            gd.VerticalLinesIP( axs[i] )   

        plt.tight_layout()
        
        try:
            progressBar.setValue(progressBar.value() + 1 )
        except AttributeError:
            pass

        if self.ShowPlots == True: plt.show()
        return

    def plotSummary(self,filename):
        # A rellenar ...
        return
    
if __name__ == "__main__":
    obj = BlmDose( rs = 9)
    obj.noise_dir_path = "/home/belen/BlmNoiseData"
    #obj.ShowPlots = False
    #obj.ComputeHistogram = False
    t1 = "2018-10-04 21:45:00"
    t2 = "2018-10-04 21:46:00"
    blmList = ['BLMEI.05L7.B2I10_TCSM.A5L7.B2', 'BLMQI.04L7.B2I10_MQWA.E4L7']
    obj.runDoseAnalysisDuringPeriod( t1, t2 ,[])
      
    #obj._mergeFilesPerFillBeamMode(7259, "STABLE", 9)





