import blmnoise
import blmtime

# MANY blm MIGHT NOT EXIST...

t0 = "2018-04-07 00:00:00" # + 230 days
#t0 = "2018-11-30 00:00:00" # 
t_final = "2018-05-01 00:00:00" # 
for i in range(30):
    t1 = blmtime.strFromTime( blmtime.timeFromStr( t0 ) + i*24*3600)
    t2 = blmtime.strFromTime( blmtime.timeFromStr( t0 ) + (i+1)*24*3600)
    if blmtime.timeFromStr(t2) > blmtime.timeFromStr(t_final):
        sys.exit()
    print("***************************")
    print(t1, t2)
    obj = blmnoise.BlmNoise( rs = 9, beamModes=["INJPROT", "NOBEAM", "SETUP","CYCLING"])
    obj.ShowPlots = False
    obj.output_dir_path = "../BlmNoiseDataCorrected_2018"
    obj.useVectorNumericBlm = True 
    obj.runNoiseAnalysisDuringPeriod( t1, t2 )
    print("***************************\n")
