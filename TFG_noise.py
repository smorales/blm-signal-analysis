import os, sys
import pytimber
from blmnoise import BlmNoise
import matplotlib.pyplot as plt
import matplotlib
db = pytimber.LoggingDB()
import blmtime

t1 = "2018-12-01 00:00:00"
t2 = "2018-12-02 00:00:00" 
# bien para el noise over todo...
#t1 = "2018-06-01 00:00:00"

#t2 = "2018-11-01 00:00:00"
#t2  = "2018-06-02 00:00:00"
#t1 = "2017-12-20 00:00:00"
#t2 = "2018-01-20 00:00:00"
#t1 = "2017-06-01 00:00:00"
#t2 = "2017-06-01 12:00:00"
#t1 = "2016-06-01 00:00:00"
#t2 = "2016-06-01 12:00:00"
#t1 = "2015-06-01 00:00:00"
#t2 = "2015-06-01 12:00:00"
t1 = "2018-10-01 12:00:00"
t2 = "2018-10-02 00:00:00" 


"""t1 = "2018-05-01 00:00:00"
#t1 = "2018-12-01 00:00:00"
t2= "2018-05-01 12:00:00"
obj = BlmNoise()
obj.output_dir_path="../BlmNoiseDataCorrected_2018"
obj.ShowPlots = False

#while blmtime.timeFromStr(t1) < blmtime.timefromStr("2018-12-20 00:00:00"):

highestSigma = {}

while blmtime.timeFromStr(t1) < blmtime.timeFromStr("2018-12-20 12:00:01"):
    obj.plotNoiseAlongRing(t1,t2)
    #print(obj.highestSigmaBLM)

    for blm in obj.highestSigmaBLM:
        try:
            highestSigma[blm] += 1
        except KeyError:
            highestSigma[blm] = 1

    t1 = t2
    t2 = blmtime.strFromTime(blmtime.timeFromStr(t2)+43200)

sort_sigma = sorted(highestSigma.items(), key=lambda x: x[1], reverse=True)
print(sort_sigma[0:30])

sys.exit()

"""


obj = BlmNoise()
obj.output_dir_path="../BlmNoiseDataCorrected_2018"
obj.ShowPlots = True
#obj.plotNoiseAlongRing(t1,t2)
blm = ["BLMTI.04L1.B2E10_TANAL.4L1","BLMQI.24L1.B2E30_MQ","BLMQI.33L1.B1I10_MQ","BLMQI.30R1.B2I10_MQ","BLMTI.04R5.B1E10_TANC.4R5","BLMQI.24R5.B2I10_MQ","BLMQI.31L5.B2E10_MQ", "BLMTI.06L7.B2I10_TCLA.A6L7.B2", "BLMQI.33L7.B2I10_MQ", "BLMQI.31R7.B1E10_MQ"]
blm = ["BLMTI.04L1.B2E10_TANAL.4L1","BLMQI.24L1.B2E30_MQ","BLMQI.33L1.B1I10_MQ","BLMQI.30R1.B2I10_MQ", "BLMTI.06L7.B2I10_TCLA.A6L7.B2", "BLMQI.33L7.B2I10_MQ", "BLMQI.31R7.B1E10_MQ"]
blm = ["BLMTI.06L7.B2I10_TCLA.A6L7.B2", "BLMQI.33L7.B2I10_MQ", "BLMQI.31R7.B1E10_MQ","BLMTI.04L1.B2E10_TANAL.4L1","BLMQI.24L1.B2E30_MQ","BLMQI.33L1.B1I10_MQ","BLMQI.30R1.B2I10_MQ" ]
blm = ["BLMTI.04L1.B2E10_TANAL.4L1","BLMQI.30R1.B2I10_MQ" ]
#blm = ["BLMQI.25L6.B1E10_MQ"]
#blm = ["BLMTI.04L1.B2E10_TANAL.4L1","BLMTI.04R5.B1E10_TANC.4R5","BLMQI.12R2.B2E30_MQ"]
#blm = ["BLMQI.33R1.B1E10_MQ","BLMQI.32R1.B2I30_MQ","BLMQI.25L3.B1I10_MQ"]
#blm = ["BLMQI.23L3.B2E10_MQ","BLMQI.30L7.B2I30_MQ","BLMQI.31R2.B1I10_MQ","BLMQI.33L2.B2I10_MQ"]
obj.plotHistogram(t1,t2,blm)
#obj.plotOffsetVsTime(t1,t2,blm)
