#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# auth: S.Morales, B.Salvachua
# date: 2019-06-05

import os,sys,glob, ast
import matplotlib
import matplotlib.transforms as transforms
import matplotlib.pyplot as plt
import matplotlib.mlab as mlab
import pytimber
import time
import pandas
import csv
import numpy as np
from datetime import datetime,timedelta
from scipy.optimize import curve_fit
import scipy.stats
import math
from blmdata import BlmData
import blmtime
import blmdb
import graphdeco as gd
from sklearn.linear_model import LinearRegression

class BlmNoise(BlmData):
    def __init__(self, energy = 6400, rs = 9, blmTypes=["IC","SEM","LIC"] , beamModes=["INJPROT", "NOBEAM", "SETUP","CYCLING"], fileBlmDcum = "lsa/20200213_BlmDbLsa.csv"):
        BlmData.__init__(self, rs, blmTypes, beamModes, fileBlmDcum )
        self.output_dir_path = "../BlmNoiseDataCorrected"

    def _setVariableNames(self):
        # Vector Numeric variables
        self.varVector_IC  = "LHC.BLMI:LOSS_RS" + '%02d' % (self.RS)
        self.varVector_SEM = "LHC.BLMS:LOSS_RS" + '%02d' % (self.RS)
        # Numeric Variables
        self.blmLossVar    = [ name+":LOSS_RS"+'%02d' % (self.RS) for name in self.blmName ]
        self.beamPresentB1 = 'HX:SMP1_PRESENT'
        self.beamPresentB2 = 'HX:SMP2_PRESENT'
        self.beamIntensityB1 = 'LHC.BCTFR.A6R4.B1:BEAM_INTENSITY'
        self.testL = ["CFV-SR"+'%d' %(number) + "-BLML:SYS_UNDER_TEST" for number in [1,2,3,5,6,7,8]]
        self.testC = ["CFV-SR"+'%d' %(number) + "-BLMC:SYS_UNDER_TEST" for number in [1,2,3,5,6,7,8]]
        self.testR = ["CFV-SR"+'%d' %(number) + "-BLMR:SYS_UNDER_TEST" for number in [1,2,3,5,6,7,8]]
        self.testI = ["CFV-SR"+'%d' %(number) + "-BLMI:SYS_UNDER_TEST" for number in [2,8]] 
        self.testE = ["CFV-SR7-BLME:SYS_UNDER_TEST"]
        self.test4 = ["CFV-SX4-BLM"+ '%s' %(letter)+":SYS_UNDER_TEST" for letter in ["L","C","R"]]
        self.all_test = self.testL + self.testC + self.testR + self.testI + self.testE + self.test4
        self.DAC_VALUES = [i.split(":")[0] + ".BLETC." + '{:0>2}'.format(f) + ":DAC_VALUES" for i in self.all_test for f in range(1,17)]
        self.variables_noBlm = [self.beamPresentB1 , self.beamPresentB2 , self.beamIntensityB1] + self.testE + self.testL + self.testC + self.testR + self.testI  + self.test4 + self.DAC_VALUES
        self.variables = self.variables_noBlm + self.blmLossVar
        return 
    
    def isTestOnGoing(self,i,data):
        for test in self.all_test:
            if data[test][i] == 1 :return True
        return False


    def excludeFillBm(self, fill, beammode):
        badfillbm = ( (7194, "NOBEAM"), # 2018 problem BLM signal in IR7
                      (3267, "SETUP") ) # problem vectornumeric metadata
        # (5936, "NOBEAM"),  # 2017
        for ifill,ibm in badfillbm:
            if fill == ifill and beammode in ibm:
                return True
        return False

    def runNoiseAnalysisDuringPeriod(self, t1, t2, userblmList = []):
        print("**********************************")
        print("** runNoiseAnalysisDuringPeriod **")
        print("** Checking times: ", t1, " -  ",t2)

        df = blmdb.get_blm_lsa_info_from_file(self.fileBlmDcum)
        self.blmName = df["MONITOR_EXP_NAME"].values
        self.blmDcum = df["DCUM"].values
        self.blmVME = df["CRATE_CMW_NAME"].values
        self.blmBLETC = df["DAB_INDEX"].values
        self.blmChannel = df["CHANNEL_INDEX"].values
        #layout#self.blmVME = df["VME_HOST_NAME"].values
        #layout#self.blmBLETC = df["BLETC_CRATE_CARD_IDX"].values
        #layout#self.blmChannel = df["BLETC_CRATE_CARD_CHAN_IDX"].values

        for imode in self.beamModes:
            #fillsbkg = list( self.getIntervalsByLHCModes(blmtime.timeFromStr(t1), blmtime.timeFromStr(t2) , imode, unixtime=True,mode1time='startTime',mode2time='endTime'))
            fillsbkg = self.getIntervalsByLHCModes(t1, t2 , imode, unixtime=True,mode1time='startTime',mode2time='endTime')
                            
            if len(fillsbkg) == 0:
                continue
            else:
                self.currentBeamMode = imode
            #data_bkg = {}
            for fill,it1s,it2s in fillsbkg:
                if self.excludeFillBm( fill, imode ): 
                    continue
                self.current_fill    = fill
                self.current_start   = it1s[0]
                self.current_end     = it2s[-1]
                print("\nFound:")
                [ print(i, fill, imode,  blmtime.strFromTime( it1s[i]) , blmtime.strFromTime( it2s[i]) ) for i in range(len(it1s)) ]
                for period in range(len(it1s)):
                    file_counter = 0
                    it1 = it1s[period]
                    it2 = it2s[period]
                    print("**********************************************************************************")
                    print("* ",self.currentBeamMode," : fill " ,fill, " from ", blmtime.strFromTime( it1 ), " to ",blmtime.strFromTime( it2 ) )
                    print("**********************************************************************************")
                    while it2 > it1:
                        # quantities that we need for mean and std per monitor
                        sum_x   = {}
                        sum_x2  = {}
                        sum_running_x2 = {}
                        self.total_n = {}
                        self.mean    = {}
                        self.std     = {}
                        #self.running_std = {}
                        self.hist = {}
                        self.DAC_INITIAL = {}
                        self.DAC_FINAL   = {}
                        # Extract data in blocks of 1hour
                        increment_time = self._secondsInHour
                        str_t1 = blmtime.strFromTime(it1)
                        str_t2 = blmtime.strFromTime( min(it2, it1 + increment_time) )
                        it1 =  it1 + increment_time  # increment time for the next loop
                      
                        tt,vv,blmList = self.getData( str_t1, str_t2 , userblmList) 
                        vv = self._filterDataNoBeamNoTest( tt, vv, blmList)
                        if len(vv["time"]) == 0: 
                            if imode == "INJPROT": break
                            else: continue
                        
                        # Calculate additional variables
                        for blm in blmList:
                            var = blm+":LOSS_RS%02d" %(self.RS) 
                            blmbit = self.blmGraysToBits[self._getBlmType(blm)]  
                            hh, bb = np.histogram( np.multiply(vv[var], 1/blmbit) , 50, ( 0, 1000) )
                            tmp_running_x2 = []
                            #for i in range( len(vv[var]) ):
                            #    nvalues = 15
                            #    tmp_running_x2 += [ vv[var][i] - np.mean(vv[var][ max(0, i-nvalues) : min( i+nvalues, len(vv[var])-1) ]) ]
                            try:
                                sum_x[blm]  += sum( vv[var] )
                                sum_x2[blm] += sum(  np.multiply(vv[var],vv[var]) )
                                #sum_running_x2[blm] += sum( np.multiply( tmp_running_x2, tmp_running_x2 ))
                                self.total_n[blm]+= len( vv[var] )
                                self.hist[blm] = [ i+j for i,j in zip(hh, self.hist[blm])]
                            except KeyError:
                                sum_x[blm]  = sum( vv[var] )
                                sum_x2[blm] = sum( np.multiply(vv[var],vv[var]) )
                                #sum_running_x2[blm] = sum( np.multiply( tmp_running_x2, tmp_running_x2 ))
                                self.total_n[blm]= len( vv[var] )
                                self.hist[blm] = [ i for i in hh]

                        self._get_dac_values(vv)                     
                        for blm in sum_x.keys():
                            self.mean[blm]  = sum_x[blm]/self.total_n[blm]
                            self.std[blm]   = math.sqrt(sum_x2[blm]/(self.total_n[blm] - 1) - self.mean[blm]*self.mean[blm])
                            #self.running_std[blm] = math.sqrt(sum_running_x2[blm]/self.total_n[blm])
 
                        # Write file every hour and Beam Mode
                        start = blmtime.strForFilenameFromTime( it1s[period] )
                        filename = self.output_dir_path+'/BlmNoise_%s_fill_%d_bm_%s_RS%02d_%02d.csv' %( start, fill, imode, self.RS, file_counter)
                        file_counter +=1 
                        if len(self.mean) != 0: 
                            self._writeFile( filename )
                            print("Output file: ",filename)
        return

    def _get_dac_values(self, data_bkg ):        
        setInitial = False
        if len(self.DAC_INITIAL) == 0: 
            setInitial = True
        for i in range(len(self.blmName)):
            BLM = self.blmName[i]
            VME = self._get_vme_host_name( self.blmVME[i] )
            BLETC = self.blmBLETC[i]
            channel = self.blmChannel[i]
            varname = "%s.BLETC.%02d:DAC_VALUES" %(VME.upper(), BLETC)
            if channel > 16:
                channel = channel - 100
                                
            if setInitial: 
                if len(data_bkg[varname][0]) == 0 :
                    #print("[Warning]:",varname, "channel index length equal 0 set DAC VALIES to -1 (",len(data_bkg[varname]), len(data_bkg[varname][0]),")")                    
                    self.DAC_INITIAL[BLM] = -1
                    self.DAC_FINAL[BLM]   = -1
                else:
                    self.DAC_INITIAL[BLM] = data_bkg[varname][0][channel-1]
                    self.DAC_FINAL[BLM]   = data_bkg[varname][-1][channel-1]

            if self.DAC_INITIAL[BLM] - self.DAC_FINAL[BLM] != 0:
                print("DAC_INITAL and DAC_FINAL not the same for monitor:",BLM,self.DAC_INITIAL[BLM]-self.DAC_FINAL[BLM] )
        return

    def _writeFile( self, filename):
        if not os.path.exists(self.output_dir_path): os.makedirs(self.output_dir_path)

        with open( filename, "w" ) as csvfile:
            fieldNames = ["FillNumber","StartTime","EndTime", "BeamMode", "BlmMonitor", "BlmDcum", "RS", "BlmOffset", "BlmStd", "BlmRunningStd", "Nsamples", "BlmDacValueInitial", "BlmDacValueFinal", "BlmGy2Bit", "BlmHistogram"]
            writer = csv.DictWriter( csvfile, fieldnames = fieldNames )
            writer.writeheader() 
            for blm in self.mean.keys():
                starttime = blmtime.strFromTime( self.current_start )
                endtime   = blmtime.strFromTime( self.current_end )
                dictforFile = {'FillNumber' : self.current_fill, 'StartTime': starttime, 'EndTime' : endtime, 'BeamMode' : self.currentBeamMode ,'RS' : "RS%02d" %(self.RS)  }               
                dictforFile["BlmDcum"]    = self._getDcum(blm)
                dictforFile["BlmMonitor"] = blm
                dictforFile["BlmOffset"]  = self.mean[blm]
                dictforFile["BlmStd"]     = self.std[blm]
                dictforFile["Nsamples"]   = self.total_n[blm]
                if dictforFile["BlmDcum"] != -1:
                    dictforFile["BlmDacValueInitial"] = self.DAC_INITIAL[blm]
                    dictforFile["BlmDacValueFinal"] = self.DAC_FINAL[blm]
                else:
                    dictforFile["BlmDacValueInitial"] = -1
                    dictforFile["BlmDacValueFinal"] = -1
                dictforFile["BlmGy2Bit"] = self.blmGraysToBits[self._getBlmType(blm)]  
                dictforFile["BlmHistogram"] = self.hist[blm]
                #dictforFile["BlmRunningStd"] = self.running_std[blm]
                dictforFile["BlmRunningStd"] = 0
                writer.writerow( dictforFile )
        return

    def _filterDataNoBeamNoTest( self, tt_data, vv_data, blmList):
        sample_size = len(tt_data)

        # create output data dictionary
        data_bkg = {}
        data_bkg["time"] = []
        data_bkg["test"] = []  
        data_bkg["CFV-testOnGoing"] = [] 
        for blm in blmList:
            data_bkg[blm+":LOSS_RS%02d" %(self.RS)] = []
        for dac in self.DAC_VALUES:
            data_bkg[dac] = []
        data_bkg_keys = [ key for key in data_bkg.keys() ]
        data_bkg_keys.remove("time")
        data_bkg_keys.remove("test")
        data_bkg_keys.remove("CFV-testOnGoing")

        print("Initial number of samples (before filtering): ", sample_size)
        if sample_size  == 0: return data_bkg

        i = -1
        while i < (sample_size-1):
            i = i+1
            if (vv_data[self.beamPresentB1][i] == 0 and vv_data[self.beamPresentB2][i] == 0):
                data_bkg["time"] += [  tt_data[i] ]
                data_bkg["test"] += [  0.5 ]   
                data_bkg["CFV-testOnGoing"] += [  self.isTestOnGoing(i , vv_data) ]
                for key in data_bkg_keys:
                    data_bkg[key] += [ vv_data[key][i] ]
            else:
                # remove 5 last values if there is beam and exit the loop
                for j in range( min(5, len(data_bkg["time"]))):
                    [ data_bkg[key].pop() for key in data_bkg.keys() ]
                break
            if self.isTestOnGoing(i , vv_data) == True and self.isTestOnGoing(i-1 , vv_data) == False:
                # remove 5 last values if there is a test starting
                for j in range( min(5, len(data_bkg["time"]))):
                    [ data_bkg[key].pop() for key in data_bkg.keys() ]

        if  len( data_bkg["time"]) < 120 : 
            data_bkg = { key : [] for key in data_bkg.keys() }

        #print(min(sample_size, len( data_bkg["time"] )))
        for i in range( sample_size):
            if i >= len( data_bkg["time"] ): break
            #print(i, sample_size, len(data_bkg["time"]) )
            if  data_bkg["CFV-testOnGoing"][i] == True:
                j = -1
                while j < 120:
                    #print("\t", j, i, sample_size, len(data_bkg["time"]))
                    j = j + 1
                    if data_bkg["CFV-testOnGoing"][i] == True: j = -1
                    [ data_bkg[key].pop(i) for key in data_bkg.keys() ] 
                    if i >= len( data_bkg["time"] ): break

        print("Final number of samples (excluding beam or HV test): ", len( data_bkg["time"] ))
        self.fig_counter += 1
        plt.figure(self.fig_counter)
        plt.title(self.currentBeamMode)
        plt.plot(tt_data, vv_data[self.beamPresentB1], "o-b", label="Beam 1")
        plt.plot(tt_data, vv_data[self.beamPresentB2], "o-r",  label="Beam 2")
        plt.plot(data_bkg["time"], data_bkg["test"], "o", color="black", label="selection")
        plt.plot(data_bkg["time"], data_bkg["CFV-testOnGoing"], "o", color="green", label="CFV test")
        blm = blmList[0]
        plt.plot(tt_data[0:sample_size], np.multiply(1e6, vv_data[ blm+":LOSS_RS%02d" %(self.RS)]), label=blm)
        plt.legend()
        plt.ylabel("No Beam = 0 / Beam = 1 / Selection = 0.5")
        pytimber.set_xaxis_date()
        plt.tight_layout()
        if self.ShowPlots == True : plt.show()
        plt.close('all')

        ## JUST FOR TESTS: plot BLM data in the list
        #self.fig_counter += 1
        #plt.figure(self.fig_counter)
        #plt.title(self.currentBeamMode)
        #for i in range( min(10, len(blmList))):
        #    blm = blmList[i]
        #    plt.plot(tt_data[0:sample_size], vv_data[ blm+":LOSS_RS%02d" %(self.RS)], label=blm)
        #plt.legend()
        #plt.ylabel("BLM signal (Gy/s)")
        #pytimber.set_xaxis_date()
        #plt.tight_layout()
        #if self.ShowPlots == True : plt.show()
        #plt.close('all')

        if  len( data_bkg["time"]) < 120 : 
            data_bkg = { key : [] for key in data_bkg.keys() }

        return data_bkg   
                             
    def runNoiseAnalysisDay(self, t1 ):
        t1_sec = blmtime.timeFromStr( t1)
        return self.runNoiseAnalysisDuringPeriod( blmtime.strFromTime( t1_sec - self._secondsInDay) , blmtime.strFromTime( t1_sec + self._secondsInDay ))


    def _gaussian(self,x,  amp, mean, sigma ):  
        return amp * np.exp( - (x-mean)**2.0 / (2 * sigma**2.0))

    def plotHistogram(self, t1,t2, blmList = [], progressBar = []):
        print("** Plot BLM noise histogram **")
        try:
            progressBar.setValue(0)

        except AttributeError:
            pass

        if len(blmList) > 25:
            print("ERROR: Too many BLMs %d, limit 25" %(len(blmList)))
            return
        if len(blmList) == 0 :
            return

        #try:
        #    progressBar.setValue(0)
        #except AttributeError:
        #    pass

        selectedList, dates = self._getBlmNoiseFileList( t1, t2 )
        #selectedList = selectedList[0:4]

        nSelected = len(selectedList)

        if nSelected == 0: 
            print("No files selected")
            return
        print("Number of files read:",nSelected)

        try:
            progressBar.setMaximum(nSelected+2)
            progressBar.setValue(1)

        except AttributeError:
            pass

        
        
        print(blmList)
        yvalues = {}
        mean = {}
        sigma = {}
        blmbit = {} 
        allsamples = {}
        for blm in blmList:
            allsamples[ blm ] = 0
            yvalues[ blm ] = []
            mean[ blm ]  = 0
            sigma[ blm ] = 0
            blmbit[blm] = self.blmGraysToBits[self._getBlmType(blm)]  

        for filename in selectedList:
            df = pandas.read_csv( filename, usecols=("BlmMonitor","BlmOffset", "BlmStd", "BlmHistogram") )
            df["BlmHistogram"] = df["BlmHistogram"].apply(ast.literal_eval)
            for blm in blmList:
                nsamples = sum( df[df["BlmMonitor"] == blm]["BlmHistogram"].values[0] )
                allsamples[blm] += nsamples
                mean[blm]  = mean[blm] + nsamples*df[df["BlmMonitor"] == blm]["BlmOffset"].values[0]
                sigma[blm] = sigma[blm] + nsamples*(df[df["BlmMonitor"] == blm]["BlmStd"].values[0])*(df[df["BlmMonitor"] == blm]["BlmStd"].values[0])
                if len(yvalues[blm]) == 0 :
                    yvalues[blm] = [ value for value in df[df["BlmMonitor"] == blm]["BlmHistogram"].values[0] ]
                else:
                    yvalues[blm] = [ v1+v2 for v1,v2 in zip(yvalues[blm], df[df["BlmMonitor"] == blm ]["BlmHistogram"].values[0]) ]
            try:
                progressBar.setValue(progressBar.value() + 1)
            except AttributeError:
                pass
        
        #print( len(yvalues[blm]) )
        xbins = np.arange(0,1000, 20)
        #xbins = np.arange(0,700,20)


        font = {'family' : 'DejaVu Sans',
                'weight' : 'normal',
                'size'   : 18}

        matplotlib.rc('font', **font)


        
        for blm in blmList:
            mean[blm] = mean[blm]/allsamples[blm]/blmbit[blm]
            sigma[blm] = math.sqrt( sigma[blm]/allsamples[blm]/blmbit[blm]/blmbit[blm] )
            mids = np.zeros(len(xbins))
            mids[:-1] = 0.5*(xbins[1:] +  xbins[:-1])
            mids[-1] = xbins[-1] + 0.5*(xbins[-1]-xbins[-2])
            #print(len(mids))
            probs = yvalues[blm]/np.sum(yvalues[blm])
            #probs = yvalues[blm]/allsamples[blm]
            np_mean = np.sum(probs*mids)
            np_std = np.sqrt(np.sum(probs*(mids-np_mean)**2))
            
            print(np.sum(yvalues[blm]))
            self.fig_counter += 1
            plt.figure(self.fig_counter, figsize = (6,5))

            """
            #Antiguo
            #plt.bar(xbins, yvalues[blm],width = 20 ,align='edge', alpha=0.5, label="avg $\mu$=%.0f $\pm$ $\sigma$=%.0f" %(mean[blm], sigma[blm] ))
            plt.bar(xbins, probs,width = 20 ,align='edge', alpha=0.5, label="avg $\mu$=%.0f $\pm$ $\sigma$=%.0f" %(mean[blm], sigma[blm] ))
            plt.plot(mids, 1/(np_std*np.sqrt(2*np.pi))*np.exp(-(mids-np_mean)**2/ (2*np_std**2)), color= "red", label = "fit $\mu$=%.0f $\pm$ $\sigma$=%.0f" %(np_mean, np_std))

            """
            
            #plt.bar(xbins, yvalues[blm],width = 20 ,align='edge', alpha=0.5, label="avg $\mu$=%.0f $\pm$ $\sigma$=%.0f" %(mean[blm], sigma[blm] ))

            plt.bar(xbins, yvalues[blm],width = 20 ,align='edge', alpha=0.5)

            #plt.bar(xbins, probs,width = 20 ,align='edge', alpha=0.5, label="avg $\mu$=%.0f $\pm$ $\sigma$=%.0f" %(mean[blm], sigma[blm] ))
            p = scipy.stats.norm.pdf(mids,np_mean, np_std)
            print(np.sum(p), np.sum(probs))
            factor = np.sum(probs)/np.sum(p)
            x = np.arange(0,1000,1)
            plt.plot(x, allsamples[blm]*factor/(np_std*np.sqrt(2*np.pi))*np.exp(-(x-np_mean)**2/ (2*np_std**2)), color= "red", label = "fit $\mu$=%.0f $\pm$ $\sigma$=%.0f" %(np_mean, np_std))
            plt.title(blm)
            plt.xlabel("CFC counts")
            plt.ylabel("Entries")
            #plt.yscale('log')
            plt.legend()

       
            """ Copiado del otro script
            #y = mlab.normpdf(xbins, np_mean, np_std)
            #plt.plot(xbins, y)
            x = np.arange(0,1000,1)
            #p = scipy.stats.norm.pdf(mids,np_mean, np_std)
            p = scipy.stats.norm.pdf(mids,np_mean, np_std)
            #plt.plot(mids, 20*p)
            #print(np.sum(p), np.sum(probs))
            factor = np.sum(probs)/np.sum(p)
            print(factor)
            #x = np.arange(0,1000,1)
            plt.plot(x, allsamples[blm]*factor/(np_std*np.sqrt(2*np.pi))*np.exp(-(x-np_mean)**2/ (2*np_std**2)), color= "red", label = "fit $\mu$=%.0f $\pm$ $\sigma$=%.0f" %(np_mean, np_std))
            #plt.plot(x, 20/(np_std*np.sqrt(2*np.pi))*np.exp(-(x-np_mean)**2/ (2*np_std**2)), color= "red", label = "fit $\mu$=%.0f $\pm$ $\sigma$=%.0f" %(np_mean, np_std))
            
            plt.title(blm)
            plt.xlabel("ADC counts")
            plt.ylabel("Entries")
            #plt.yscale('log')
            plt.legend()"""




        plt.tight_layout()
        plt.show()
        try:
            progressBar.setValue(progressBar.value() + 1)
        except AttributeError:
            pass
        return

    def _getBlmNoiseFileList( self, t1, t2, beamModes = [""] ):
        # List of files with noise analysis
        listInputNoiseFiles = []
        for imode in beamModes : 
            listInputNoiseFiles += glob.glob(self.output_dir_path+"/BlmNoise_*"+imode+"*.csv")
        listInputNoiseFiles.sort()
        if len(listInputNoiseFiles) == 0:
            print("No files in directory: ", self.output_dir_path)
            return [],[]
        

        # Search files between dates
        # Compare t1 with ti_file in order to select files
        t1_sec = blmtime.timeFromStr(t1)
        t2_sec = blmtime.timeFromStr(t2)
        selectedList=[]
        dates = []
        for f in listInputNoiseFiles:
            #print(f)
            t1_file = (f.split(".csv")[0]).split("BlmNoise_")[1]
            t1_file = t1_file.split("_fill")[0]
            #print((f.split(".csv")[0]).split("_"))

            # Solo porque la carpeta tiene un _ de mas
            #t1_file_number = (f.split(".csv")[0]).split("_")[8]
            t1_file_number = (f.split(".csv")[0]).split("_")[9]

            t1_file_sec =  blmtime.timeFromFilenameFromStr(t1_file) + 3600*float(t1_file_number)
            #print(t1_file)
            if t1_file_sec < t1_sec: continue
            if t1_file_sec > t2_sec: break
            selectedList.append( f )
            dates += [t1_file_sec]
            print(f)
        return selectedList, dates


    def plotOffsetVsTime( self, t1, t2, blmList = [], showDACValues = True , progressBar = []):
        self.plotNoiseVsTime(t1, t2, blmList = blmList, yvariable = "BlmOffset", ylabel = "BLM Offset", showDACValues = showDACValues, progressBar = progressBar) 
        return


    def plotSigmaVsTime( self, t1, t2, blmList = [], showDACValues = False, progressBar = [] ):
        self.plotNoiseVsTime(t1, t2, blmList = blmList, yvariable = "BlmStd", ylabel = "BLM Std Deviation", showDACValues = showDACValues, progressBar=progressBar) 
        return


    def plotNoiseVsTime(self, t1, t2, blmList = [], yvariable = "BlmOffset", ylabel = "BLM Offset", showDACValues = True, progressBar = []):
        print("** Plot BLM noise vs time **")
        try:
            progressBar.setValue(0)

        except AttributeError:
            pass
        selectedList, dates = self._getBlmNoiseFileList( t1, t2 )
           
        

        nSelected = len(selectedList)
        if nSelected == 0: 
            print("No files selected")
            return
        print("Number of files read:",nSelected)

        try:
            progressBar.setMaximum(2*len(blmList) + 4)
            progressBar.setValue(1)
        except AttributeError:
            pass

        # Create Pandas DataFrame of concat files
        df = pandas.concat(map(pandas.read_csv, selectedList ))
        

        try:
            progressBar.setValue(progressBar.value() +1 )
        except AttributeError:
            pass


        #self.fig_size = (10,6)

        blmListIC = []
        blmListLIC = []
        blmListSEM = []

        for blm in blmList:
            if self._getBlmType(blm)=="IC" or self._getBlmType(blm)=="DUMP_IC":
                blmListIC += [ blm ] 

            if self._getBlmType(blm)=="LIC" or self._getBlmType(blm)=="DUMP_LIC":
                blmListLIC += [ blm ]    
            
            if self._getBlmType(blm)=="SEM" or self._getBlmType(blm)=="DUMP_SEM":
                blmListSEM += [ blm ] 

        try:
            progressBar.setValue(progressBar.value() +1 )
        except AttributeError:
            pass

        if len(blmListIC)>0:

            #self.fig_counter += 1
            #self.fig_size = (12,6)
            #figIC, host = plt.subplots(num=self.fig_counter,figsize = self.fig_size)
            #figIC.subplots_adjust(left=0.15)
        
            #if showDACValues:
            #    par1 = host.twinx()
            #    par1.set_ylabel("DAC Value")
            #par2 = host.twinx()

            #par2.yaxis.tick_left()
            #par2.spines["left"].set_position(("axes", -0.08))
        
            # Mirar aqui, cambio para lo otro...
            font = {'family' : 'DejaVu Sans','weight' : 'normal','size'   : 20}
            matplotlib.rc('font', **font)
            for blm in blmListIC:
                self.fig_counter += 1
                self.fig_size = (12,6)
                figIC, host = plt.subplots(num=self.fig_counter,figsize = self.fig_size)
                figIC.subplots_adjust(left=0.15)
        
                if showDACValues:
                    par1 = host.twinx()
                    par1.set_ylabel("DAC Value")
                    #par2 = host.twinx()

                    #par2.yaxis.tick_left()
                    #par2.spines["left"].set_position(("axes", -0.08))


                host.set_yscale('log')
                host.plot(dates, df[ df["BlmMonitor"] == blm][yvariable],"o" ,label=blm)
                try:
                    progressBar.setValue(progressBar.value() +1 )
                except AttributeError:
                    pass
                #par2.plot( dates, np.multiply(df[ df["BlmMonitor"] == blm][yvariable], 1/(df[ df["BlmMonitor"] == blm]["BlmGy2Bit"])),"*", label ="Bits "+blm)
            

                if showDACValues:
                    par1.plot(dates, df[df["BlmMonitor"]==blm]["BlmDacValueInitial"],"-",color='green',label="DAC "+blm)
                try:
                    progressBar.setValue(progressBar.value() +1 )
                except AttributeError:
                    pass

                host.set_ylabel(ylabel + " (Gy/s)")
        
                #par2.set_ylabel(ylabel + " (Bits/s)")
                #par2.yaxis.set_label_position("left")

                #host.ticklabel_format(axis='y', style='sci', scilimits=(-3,3))
                pytimber.set_xaxis_date()
            #host.set_title("From %s to %s for RS%02d" %(t1.split()[0], t2.split()[0], self.RS ))

            #host.set_ylabel(ylabel + " (Gy/s)")
        
            #par2.set_ylabel(ylabel + " (Bits/s)")
            #par2.yaxis.set_label_position("left")

            #host.ticklabel_format(axis='y', style='sci', scilimits=(-3,3))
            #lines = [p1]
            #if showDACValues:
            #    lines = [p1, p2]

            #host.legend()
        

            #pytimber.set_xaxis_date()


        if len(blmListLIC)>0:

            self.fig_counter += 1
            self.fig_size = (12,6)
            figLIC, host = plt.subplots(num=self.fig_counter,figsize = self.fig_size)
            figLIC.subplots_adjust(left=0.15)
        
            if showDACValues:
                par1 = host.twinx()
                par1.set_ylabel("DAC Value")
            par2 = host.twinx()

            par2.yaxis.tick_left()
            par2.spines["left"].set_position(("axes", -0.08))
        

            for blm in blmListLIC:
                host.plot(dates, df[ df["BlmMonitor"] == blm][yvariable],"o" ,label=blm)
                try:
                    progressBar.setValue(progressBar.value() +1 )
                except AttributeError:
                    pass
                par2.plot( dates, np.multiply(df[ df["BlmMonitor"] == blm][yvariable], 1/(df[ df["BlmMonitor"] == blm]["BlmGy2Bit"])),"*", label ="Bits "+blm)
            


                if showDACValues:
                    par1.plot(dates, df[df["BlmMonitor"]==blm]["BlmDacValueInitial"],"-",label="DAC "+blm)


                try:
                    progressBar.setValue(progressBar.value() +1 )
                except AttributeError:
                    pass

            host.set_title("From %s to %s for RS%02d" %(t1.split()[0], t2.split()[0], self.RS ))

            host.set_ylabel(ylabel + " (Gy/s)")
        
            par2.set_ylabel(ylabel + " (Bits/s)")
            par2.yaxis.set_label_position("left")

            host.ticklabel_format(axis='y', style='sci', scilimits=(-3,3))
            #lines = [p1]
            #if showDACValues:
            #    lines = [p1, p2]

            host.legend()
        

            pytimber.set_xaxis_date()



        if len(blmListSEM)>0:

            self.fig_counter += 1
            self.fig_size = (12,6)
            figSEM, host = plt.subplots(num=self.fig_counter,figsize = self.fig_size)
            figSEM.subplots_adjust(left=0.15)
        
            if showDACValues:
                par1 = host.twinx()
                par1.set_ylabel("DAC Value")
            par2 = host.twinx()

            par2.yaxis.tick_left()
            par2.spines["left"].set_position(("axes", -0.08))
        

            for blm in blmListSEM:
                host.plot(dates, df[ df["BlmMonitor"] == blm][yvariable],"o" ,label=blm)
                try:
                    progressBar.setValue(progressBar.value() +1 )
                except AttributeError:
                    pass
                par2.plot( dates, np.multiply(df[ df["BlmMonitor"] == blm][yvariable], 1/(df[ df["BlmMonitor"] == blm]["BlmGy2Bit"])),"*", label ="Bits "+blm)
            


                if showDACValues:
                    par1.plot(dates, df[df["BlmMonitor"]==blm]["BlmDacValueInitial"],"-",label="DAC "+blm)

                try:
                    progressBar.setValue(progressBar.value() +1 )
                except AttributeError:
                    pass

            host.set_title("From %s to %s for RS%02d" %(t1.split()[0], t2.split()[0], self.RS ))

            host.set_ylabel(ylabel + " (Gy/s)")
        
            par2.set_ylabel(ylabel + " (Bits/s)")
            par2.yaxis.set_label_position("left")

            host.ticklabel_format(axis='y', style='sci', scilimits=(-3,3))
            #lines = [p1]
            #if showDACValues:
            #    lines = [p1, p2]

            host.legend()
        

            pytimber.set_xaxis_date()

        
        if self.ShowPlots == True: plt.show() 


        try:
            progressBar.setValue(progressBar.value() +1 )
        except AttributeError:
            pass

        return

    def plotNoiseAlongRing( self, t1, t2, blmList = [], beamModes = [""], progressBar = []):
        keyOffset = "BlmOffset"
        keySigma  = "BlmStd"
        #keySigma = "BlmRunningStd"

        print("** Plot BLM noise vs Position **")
        try:
            progressBar.setValue(0)

        except AttributeError:
            pass

        # if blmList is empty then use all Blm in the default file
        # if blmList is not empty use the BLMs in the list
        selectedList, dates = self._getBlmNoiseFileList( t1, t2, beamModes = beamModes )
        #selectedList = selectedList[0:1]

        nSelected = len(selectedList)

        if nSelected == 0: 
            print("ERROR: No files selected")
            return
        print("Number of files read:",nSelected) 
        
        try:
            progressBar.setMaximum(nSelected + 7) 
            progressBar.setValue(1)
        except AttributeError:
            pass



        # Create Pandas DataFrame for OFFSET
        for filename in selectedList:
            #print("Reading 1: ", filename)
            df = pandas.read_csv( filename )
            #df = df[ [ self._getBlmType(blm)  == "SEM" for blm in df["BlmMonitor"] ] ].reset_index()
            #print(df)
            try:
                nsamples += [ df["Nsamples"][0] ]
                avg_mean = avg_mean + df[keyOffset]*nsamples[-1]
                n_sigma_sigma = n_sigma_sigma + df[keySigma]*df[keySigma]*(nsamples[-1]-1)
            except UnboundLocalError:
                nsamples = [ df["Nsamples"][0] ]
                avg_mean =  df[keyOffset]*nsamples[-1]
                n_sigma_sigma = df[keySigma]*df[keySigma]*(nsamples[-1]-1)
                

            try:
                progressBar.setValue(progressBar.value() + 1)
  
            except AttributeError:
                pass

        avg_mean = avg_mean/sum(nsamples)
        avg_sigma = np.sqrt(  n_sigma_sigma/sum(nsamples) )

        try:
            progressBar.setValue(progressBar.value() + 1)
        except AttributeError:
            pass

        
        ## ADD CRATE NUMER AND CARD NUMBER
        #avg_ratio_sigma_mean = avg_sigma/avg_mean
        avg_ratio_mean_sigma = avg_mean/avg_sigma
        #avg_ratio_cut = 4e-1        # only for IC (the rest do not show a clear distribution)
        avg_ratio_cut = 7
        df2 = blmdb.get_blm_lsa_info_from_file(self.fileBlmDcum)
        print(" ***** BLM Names with high noise ***** ")
        #highestSigmaBLM = df["BlmMonitor"][ avg_ratio_sigma_mean > avg_ratio_cut].values
        highestSigmaBLM = df["BlmMonitor"][ avg_ratio_mean_sigma < avg_ratio_cut].values
        

        for blm in highestSigmaBLM:
            try:
                print( "%30s" %(blm), "->", "%12s" %(df2[df2["MONITOR_EXP_NAME"] == blm]["CRATE_CMW_NAME"].values[0]), 
                       "->","BLETC (proc.surface):", "%2d" %(df2[df2["MONITOR_EXP_NAME"] == blm]["DAB_INDEX"].values[0]), 
                       "->","BLECF (acq. tunnel) :", "%3d"  %(df2[df2["MONITOR_EXP_NAME"] == blm]["BLECF_SERIAL"].values[0]),
                       "->","Offset:", "%.2g" %(df[df["BlmMonitor"] == blm][keyOffset].values[0]), 
                       "->","Std:", "%.2g" %(df[df["BlmMonitor"] == blm][keySigma].values[0]))
            except ValueError:
                print( "%30s" %(blm), "->", "%12s" %(df2[df2["MONITOR_EXP_NAME"] == blm]["CRATE_CMW_NAME"].values[0]), 
                       "->","BLETC (proc.surface):", "%2d" %(df2[df2["MONITOR_EXP_NAME"] == blm]["DAB_INDEX"].values[0]), 
                       "->","BLECF EMPTY (tunnel):   ",
                       "->","Offset:", "%.2g" %(df[df["BlmMonitor"] == blm][keyOffset].values[0]), 
                       "->","Std:", "%.2g" %(df[df["BlmMonitor"] == blm][keySigma].values[0]))


            except IndexError:
                print( "%30s" %(blm), "->", "Not in the database", 
                       "->","Offset:", "%.2g" %(df[df["BlmMonitor"] == blm][keyOffset].values[0]), 
                       "->","Std:", "%.2g" %(df[df["BlmMonitor"] == blm][keySigma].values[0]))
        print(" ************************************* ")


        try:
            progressBar.setValue(progressBar.value() + 1)
        except AttributeError:
            pass
            

        """
        self.fig_counter += 1
        font = {'family' : 'DejaVu Sans','weight' : 'normal','size'   : 20}
        matplotlib.rc('font', **font)
        plt.figure(self.fig_counter, figsize = (6,5))
        plt.plot(avg_mean, avg_sigma, ".")
        plt.plot( avg_mean[ avg_ratio_sigma_mean > avg_ratio_cut ] , avg_sigma[ avg_ratio_sigma_mean > avg_ratio_cut ], ".")        
        plt.yscale('log')
        plt.xscale('log')
        plt.xlabel("BLM Offset [Gy/s]")
        plt.ylabel("BLM Std Deviation [Gy/s]")
        """
        
        ## Add to df the calculated quantitites
        #print(avg_sigma)
        df["Avg_Mean"]  = avg_mean
        df["Avg_Sigma"] = avg_sigma
        #print(df["Avg_Sigma"])
        
        #df = pandas.concat(map(pandas.read_csv, selectedList ))
        #print("DF concatenated")

        # Reduce the DataFrame including only selected blm in the blmList
        if len(blmList) != 0 : df = df[ [ i in blmList for i in df["BlmMonitor"] ] ]
        #print("Reduce DF size if blmList")

        # BLN: verify this!
        # BLN: What do we do wiht the DUMP and TL ?
        df_ic = df[ [ self._getBlmType(blm)  == "IC" for blm in df["BlmMonitor"] ] ]
        df_lic = df[ [ self._getBlmType(blm) == "LIC" for blm in df["BlmMonitor"] ] ]
        df_sem = df[ [ self._getBlmType(blm) == "SEM" for blm in df["BlmMonitor"] ] ]
        df_dump_ic = df[ [ self._getBlmType(blm) == "DUMP_IC" for blm in df["BlmMonitor"] ] ]
        df_dump_lic = df[ [ self._getBlmType(blm) == "DUMP_LIC" for blm in df["BlmMonitor"] ] ]
        df_dump_sem = df[ [ self._getBlmType(blm) == "DUMP_SEM" for blm in df["BlmMonitor"] ] ]
        
        
        """
        self.fig_counter += 1
        plt.figure(self.fig_counter, figsize = (6,5))
        plt.plot(avg_mean, avg_sigma/avg_mean, ".")
        plt.xlabel("BLM Offset [Gy/s]")
        plt.ylabel("Sigma/Mean")        
        #plt.yscale('log')
        #plt.xscale('log')

        self.fig_counter += 1
        plt.figure(self.fig_counter, figsize = (6,5))
        plt.plot(avg_mean, np.log10(avg_sigma)/np.log10(avg_mean), ".")
        plt.xlabel("BLM Offset [Gy/s]")
        plt.ylabel("Sigma/Mean")        
        #plt.xscale('log')
        

        self.fig_counter += 1
        plt.figure(self.fig_counter, figsize = (6,5))
        plt.hist(df_ic["Avg_Mean"], 100)
        plt.xlabel("BLM Offset [Gy/s]")
        plt.title("Avgoffset_hist")
        plt.yscale('log')
        plt.xscale('log')

        self.fig_counter += 1
        plt.figure(self.fig_counter, figsize = (6,5))
        plt.hist(df_ic["Avg_Sigma"], 100)
        plt.xlabel("BLM Std Deviation [Gy/s]")
        plt.title("Avgsigma_hist")
        plt.yscale('log')
        plt.xscale('log')

        
        self.fig_counter += 1
        plt.figure(self.fig_counter, figsize = (6,5))
        plt.plot(df_ic["BlmDcum"], df_ic["Avg_Sigma"]/df_ic["Avg_Mean"], ".")
        plt.title("Avgsigma/avg mean en dcum")
        #plt.yscale('log')
        #plt.xscale('log')
        """
        

        
        
        try:
            progressBar.setValue(progressBar.value() + 1)
        except AttributeError:
            pass

        
        # PLOT BLM vs SPOS
        font = {'family' : 'DejaVu Sans','weight' : 'normal','size'   : 20}
        matplotlib.rc('font', **font)
        self.fig_size = (15,6)
        self.fig_counter += 1
        plt.figure(self.fig_counter, self.fig_size)
        #fig, axs = plt.subplots(2,2,sharex=True,gridspec_kw={'hspace': 0},num = self.fig_counter)
        #fig, axs = plt.subplots(2,2,sharex=True, figsize = self.fig_size , gridspec_kw={'hspace': 0},num = self.fig_counter)
        fig, axs = plt.subplots(2,1,sharex=True, figsize = self.fig_size , gridspec_kw={'hspace': 0},num = self.fig_counter)
        #fig.suptitle("From %s to %s for RS%02d" %(t1.split()[0], t2.split()[0], self.RS ))

        # OFFSET
        if len(df_ic) != 0:
            #axs[0,0].errorbar(df_ic["BlmDcum"], df_ic["Avg_Mean"], yerr = df_ic["Avg_Sigma"], capsize = 4, fmt="ob", markersize=2, label="IC")        

            axs[0].errorbar(df_ic["BlmDcum"], df_ic["Avg_Mean"], yerr = df_ic["Avg_Sigma"], capsize = 4, fmt="ob", markersize=2, label="IC") 
            #axs[0].errorbar(df_ic["BlmDcum"], df_ic["Avg_Mean"], capsize = 4, fmt="ob", markersize=2, label="IC") 


        if len(df_lic) != 0:
            #axs[0,0].errorbar(df_lic["BlmDcum"], df_lic["Avg_Mean"], yerr = df_lic["Avg_Sigma"], capsize = 4, fmt="og",  markersize=2,label="LIC")
            axs[0].errorbar(df_lic["BlmDcum"], df_lic["Avg_Mean"], yerr = df_lic["Avg_Sigma"], capsize = 4, fmt="og",  markersize=2,label="LIC")
            #axs[0].errorbar(df_lic["BlmDcum"], df_lic["Avg_Mean"], capsize = 4, fmt="og",  markersize=2,label="LIC")



        #if len(df_dump_ic) != 0:
            #axs[0,0].errorbar(-(df_dump_ic["BlmDcum"]), df_dump_ic["Avg_Mean"], yerr = df_dump_ic["Avg_Sigma"], capsize = 4, fmt="or",  markersize=2,label="DUMP IC")
            #axs[0].errorbar(-(df_dump_ic["BlmDcum"]), df_dump_ic["Avg_Mean"], yerr = df_dump_ic["Avg_Sigma"], capsize = 4, fmt="or",  markersize=2,label="DUMP IC")
            #axs[0].errorbar(-(df_dump_ic["BlmDcum"]), df_dump_ic["Avg_Mean"], capsize = 4, fmt="or",  markersize=2,label="DUMP IC")


        #if len(df_dump_lic) !=0:
            #axs[0,0].errorbar(-(df_dump_lic["BlmDcum"]), df_dump_lic["Avg_Mean"], yerr = df_dump_lic["Avg_Sigma"], capsize = 4, fmt="ok",  markersize=2,label="DUMP LIC")
            #axs[0].errorbar(-(df_dump_lic["BlmDcum"]), df_dump_lic["Avg_Mean"], yerr = df_dump_lic["Avg_Sigma"], capsize = 4, fmt="ok",  markersize=2,label="DUMP LIC")
            #axs[0].errorbar(-(df_dump_lic["BlmDcum"]), df_dump_lic["Avg_Mean"], capsize = 4, fmt="ok",  markersize=2,label="DUMP LIC")


        if len(df_sem) != 0:
            #axs[0,1].errorbar(df_sem["BlmDcum"], df_sem["Avg_Mean"], yerr = df_sem["Avg_Sigma"] ,capsize = 4, fmt="om",  markersize=2,label="SEM")
            axs[1].errorbar(df_sem["BlmDcum"], df_sem["Avg_Mean"], yerr = df_sem["Avg_Sigma"] ,capsize = 4, fmt="om",  markersize=2,label="SEM")
            #axs[1].errorbar(df_sem["BlmDcum"], df_sem["Avg_Mean"],capsize = 4, fmt="om",  markersize=2,label="SEM")


        #if len(df_dump_sem) != 0:
            #axs[0,1].errorbar(-(df_dump_sem["BlmDcum"]), df_dump_sem["Avg_Mean"], yerr = df_dump_sem["Avg_Sigma"] ,capsize = 4, fmt="oc",  markersize=2,label="DUMP SEM")
            #axs[1].errorbar(-(df_dump_sem["BlmDcum"]), df_dump_sem["Avg_Mean"], yerr = df_dump_sem["Avg_Sigma"] ,capsize = 4, fmt="oc",  markersize=2,label="DUMP SEM")
            #axs[1].errorbar(-(df_dump_sem["BlmDcum"]), df_dump_sem["Avg_Mean"],capsize = 4, fmt="oc",  markersize=2,label="DUMP SEM")



        #axs[0,0].set(ylabel='BLM Offset [Gy/s]',xlabel='Position in the LHC [m]')
        #axs[0,1].set(ylabel='BLM Offset [Gy/s]',xlabel='Position in the LHC [m]')


        #axs[0].set(ylabel='BLM Offset [Gy/s]',xlabel='Position in the LHC [m]')
        #axs[1].set(ylabel='BLM Offset [Gy/s]',xlabel='Position in the LHC [m]')

        axs[1].set_xlabel("Position in the LHC [m]")
        fig.text(0.015, 0.5, 'BLM Offset [Gy/s]', ha='center', va='center', rotation='vertical')


        axs[0].set_yscale('log')
        axs[1].set_yscale('log')

        gd.VerticalLinesIP( axs[0] ) 
        gd.VerticalLinesIP( axs[1] )



        #max_offset = 1e-5
        #max_sigma  = 1e-6
        #for idf in [df_ic,df_lic,df_dump_ic,df_dump_lic]:
        #    for index,row in (idf).iterrows():
        #        if row["Avg_Mean"] > max_offset or row["Avg_Sigma"] > max_sigma :
        #            print("IC/LIC: %20s  Offset  %10g  Sigma %10g Dcum%10g " %(row["BlmMonitor"],row["Avg_Mean"], row["Avg_Sigma"], row["BlmDcum"]))
        #max_offset = 1e-1
        #max_sigma  = 1e-2
        #for idf in [df_sem,df_dump_sem]:
        #    for index,row in (idf).iterrows():
        #        if row["Avg_Mean"] > max_offset or row["Avg_Sigma"] > max_sigma :
        #            print("SEM:    %20s  Offset  %10g  Sigma %10g Dcum%10g " %(row["BlmMonitor"],row["Avg_Mean"], row["Avg_Sigma"], row["BlmDcum"]))


        try:
            progressBar.setValue(progressBar.value() + 1)
        except AttributeError:
            pass


        # SIGMA 
        self.fig_counter += 1
        plt.figure(self.fig_counter, self.fig_size)
        #fig, axs = plt.subplots(2,2,sharex=True,gridspec_kw={'hspace': 0},num = self.fig_counter)
        #fig, axs = plt.subplots(2,2,sharex=True, figsize = self.fig_size , gridspec_kw={'hspace': 0},num = self.fig_counter)
        fig, axs = plt.subplots(2,1,sharex=True, figsize = self.fig_size , gridspec_kw={'hspace': 0},num = self.fig_counter)
        #fig.suptitle("From %s to %s for RS%02d" %(t1.split()[0], t2.split()[0], self.RS ))
        if len(df_ic) != 0:
            #axs[1,0].errorbar(df_ic["BlmDcum"], df_ic["Avg_Sigma"], capsize = 4, fmt="ob",  markersize=2,label="IC")
            #axs[0].errorbar(df_ic["BlmDcum"], df_ic["Avg_Sigma"], capsize = 4, fmt="ob",  markersize=2,label="IC")
            #axs[0].errorbar(df_ic["BlmDcum"], df_ic["Avg_Sigma"]/df_ic["Avg_Mean"], capsize = 4, fmt="ob",  markersize=2,label="IC")
            axs[0].errorbar(df_ic["BlmDcum"], df_ic["Avg_Mean"]/df_ic["Avg_Sigma"], capsize = 4, fmt="ob",  markersize=2,label="IC")
        if len(df_lic) != 0:
            #axs[1,0].errorbar(df_lic["BlmDcum"], df_lic["Avg_Sigma"], capsize = 4, fmt="og",  markersize=2,label="LIC")
            #axs[0].errorbar(df_lic["BlmDcum"], df_lic["Avg_Sigma"]/df_lic["Avg_Mean"], capsize = 4, fmt="og",  markersize=2,label="LIC")
            axs[0].errorbar(df_lic["BlmDcum"], df_lic["Avg_Mean"]/df_lic["Avg_Sigma"], capsize = 4, fmt="og",  markersize=2,label="LIC")

        #if len(df_dump_ic) != 0:
            #axs[1,0].errorbar(-(df_dump_ic["BlmDcum"]), df_dump_ic["Avg_Sigma"], capsize = 4, fmt="or",  markersize=2,label="DUMP IC")
        #if len(df_dump_lic) != 0:
            #axs[1,0].errorbar(-(df_dump_lic["BlmDcum"]), df_dump_lic["Avg_Sigma"], capsize = 4, fmt="ok",  markersize=2,label="DUMP LIC")

        #axs[0,0].set_yscale('log')
        #axs[0,1].set_yscale('log')
        #axs[1,0].set_yscale('log')
        #axs[1,1].set_yscale('log')

        axs[0].set_yscale('log')
        axs[1].set_yscale('log')

        if len(df_sem) != 0:
            #axs[1,1].errorbar(df_sem["BlmDcum"], df_sem["Avg_Sigma"], capsize = 4, fmt="om",  markersize=2,label="SEM")
            #axs[1].errorbar(df_sem["BlmDcum"], df_sem["Avg_Sigma"]/df_sem["Avg_Mean"], capsize = 4, fmt="om",  markersize=2,label="SEM")
            axs[1].errorbar(df_sem["BlmDcum"], df_sem["Avg_Mean"]/df_sem["Avg_Sigma"], capsize = 4, fmt="om",  markersize=2,label="SEM")

        #if len(df_dump_sem) !=0:
            #axs[1,1].errorbar(-(df_dump_sem["BlmDcum"]), df_dump_sem["Avg_Sigma"], capsize = 4, fmt="oc",  markersize=2,label="DUMP SEM")            

        #plt.yscale('log')
        
        fig.text(0.015, 0.5, 'BLM Offset / BLM Std Deviation', ha='center', va='center', rotation='vertical')


        gd.VerticalLinesIP( axs[0] ) 
        gd.VerticalLinesIP( axs[1] )
        #axs[1,0].set(ylabel='BLM Std Deviation [Gy/s]',xlabel='Position in the LHC [m]')
        #axs[1,1].set(ylabel='BLM Std Deviation [Gy/s]',xlabel='Position in the LHC [m]')
        
        

        try:
            progressBar.setValue(progressBar.value() + 1)
        except AttributeError:
            pass


        #twissDataDict = gd.GetMadElements()
        #for i,j in zip( (0,0,1,1), (0,1,0,1) ):
        #    axs[i,j].set_yscale('log')
        #    axs[i,j].grid(axis='y')
        #    axs[i,j].legend()
        #    gd.VerticalLinesIP( axs[i,j] )   
        #    gd.MadElementsVerticalRectangle(axs[i,j], twissDataDict)         


        #from mpldatacursor import datacursor
        #datacursor(axs[1,0], hover=True, formatter='customer: {y:0.0f}'.format)
        #import mplcursors
        #mplcursors.cursor( axs[1,0] )
        #def fmt(**dic):
        #    tx = '$T=${x:.2f}$^\circ$C\n$I=${y:.2f}$\,$mA\n$\Delta F=${z:.2f}$\,$THz'
        #    dic.update({"z" : c[dic["ind"][0]]})
        #    return tx.format(**dic)
        #datacursor(axs[1,0], formatter=fmt, draggable=True)
        
        

        """
        x = [ val for val in df_ic["BlmDcum"].values]
        y = [ val for val in df_ic["Avg_Sigma"].values]        
        name = [ val for val in df_ic["BlmMonitor"].values ]
        def onpick3(event):
            ind = event.ind
            ind = ind[-1]
            print('onpick3 scatter:', ind, np.take(x, ind), np.take(y, ind), np.take(name,ind) )  
            #axs[1,0].annotate.set_text(np.take(name,ind))
            axs[1,0].text( np.take(x,ind), np.take(y,ind), np.take(name,ind), fontsize = 5, rotation = 90 )
            fig.canvas.draw()
            # update
        #def onpick(event):
        #thisline = event.artist
        #xdata = thisline.get_xdata()
        #ydata = thisline.get_ydata()
        #ind = event.ind
        #print 'onpick points:', zip(xdata[ind], ydata[ind])

        fig.canvas.mpl_connect('pick_event', onpick3)
        """
        """def onhover(event):
            name = ""
            for index,row in (df).iterrows():
                if abs( row["BlmDcum"] - event.target[0]) < 0.2:
                    name = row["BlmMonitor"]
                    break
            if len(name) != 0 :
                event.annotation.set_text(name)

        import mplcursors
        crs= []
        for i,j in zip( (0,0,1,1), (0,1,0,1) ):
            crs += [ mplcursors.cursor(axs[i,j], hover= True) ]
            crs[-1].connect("add", onhover)"""

        #plt.tight_layout()
        if self.ShowPlots == True: plt.show()
        try:
            progressBar.setValue(progressBar.value() + 1)
        except AttributeError:
            pass
        return 


    def plotOffsetIC(self, t1, t2, blmList = [], beamModes = [""]):
        # Only plotting for the paper contribution



        keyOffset = "BlmOffset"
        keySigma  = "BlmStd"

        print("** Plot BLM noise vs Position **")
    

        # if blmList is empty then use all Blm in the default file
        # if blmList is not empty use the BLMs in the list
        selectedList, dates = self._getBlmNoiseFileList( t1, t2, beamModes = beamModes )
        #selectedList = selectedList[0:1]

        nSelected = len(selectedList)

        if nSelected == 0: 
            print("ERROR: No files selected")
            return
        print("Number of files read:",nSelected) 
        



        # Create Pandas DataFrame for OFFSET
        for filename in selectedList:
            #print("Reading 1: ", filename)
            df = pandas.read_csv( filename )
            #df = df[ [ self._getBlmType(blm)  == "SEM" for blm in df["BlmMonitor"] ] ].reset_index()
            #print(df)
            try:
                nsamples += [ df["Nsamples"][0] ]
                avg_mean = avg_mean + df[keyOffset]*nsamples[-1]
                n_sigma_sigma = n_sigma_sigma + df[keySigma]*df[keySigma]*(nsamples[-1]-1)
            except UnboundLocalError:
                nsamples = [ df["Nsamples"][0] ]
                avg_mean =  df[keyOffset]*nsamples[-1]
                n_sigma_sigma = df[keySigma]*df[keySigma]*(nsamples[-1]-1)
                

            

        avg_mean = avg_mean/sum(nsamples)
        avg_sigma = np.sqrt(  n_sigma_sigma/sum(nsamples) )

        


        ## ADD CRATE NUMER AND CARD NUMBER
        #avg_ratio_sigma_mean = avg_sigma/avg_mean
        avg_ratio_mean_sigma = avg_mean/avg_sigma
        #avg_ratio_cut = 4e-1        # only for IC (the rest do not show a clear distribution)
        avg_ratio_cut = 6
        df2 = blmdb.get_blm_lsa_info_from_file(self.fileBlmDcum)
        print(" ***** BLM Names with high noise ***** ")
        #highestSigmaBLM = df["BlmMonitor"][ avg_ratio_sigma_mean > avg_ratio_cut].values
        highestSigmaBLM = df["BlmMonitor"][ avg_ratio_mean_sigma < avg_ratio_cut].values
        
        for blm in highestSigmaBLM:
            try:
                print( "%30s" %(blm), "->", "%12s" %(df2[df2["MONITOR_EXP_NAME"] == blm]["CRATE_CMW_NAME"].values[0]), 
                       "->","BLETC (proc.surface):", "%2d" %(df2[df2["MONITOR_EXP_NAME"] == blm]["DAB_INDEX"].values[0]), 
                       "->","BLECF (acq. tunnel) :", "%3d"  %(df2[df2["MONITOR_EXP_NAME"] == blm]["BLECF_SERIAL"].values[0]),
                       "->","Offset:", "%.2g" %(df[df["BlmMonitor"] == blm][keyOffset].values[0]), 
                       "->","Std:", "%.2g" %(df[df["BlmMonitor"] == blm][keySigma].values[0]))
            except ValueError:
                print( "%30s" %(blm), "->", "%12s" %(df2[df2["MONITOR_EXP_NAME"] == blm]["CRATE_CMW_NAME"].values[0]), 
                       "->","BLETC (proc.surface):", "%2d" %(df2[df2["MONITOR_EXP_NAME"] == blm]["DAB_INDEX"].values[0]), 
                       "->","BLECF EMPTY (tunnel):   ",
                       "->","Offset:", "%.2g" %(df[df["BlmMonitor"] == blm][keyOffset].values[0]), 
                       "->","Std:", "%.2g" %(df[df["BlmMonitor"] == blm][keySigma].values[0]))


            except IndexError:
                print( "%30s" %(blm), "->", "Not in the database", 
                       "->","Offset:", "%.2g" %(df[df["BlmMonitor"] == blm][keyOffset].values[0]), 
                       "->","Std:", "%.2g" %(df[df["BlmMonitor"] == blm][keySigma].values[0]))
        print(" ************************************* ")


        
        self.fig_counter += 1
        plt.figure(self.fig_counter, figsize = (6,5))
        plt.plot(avg_mean, avg_sigma, ".")
        plt.plot( avg_mean[ avg_ratio_sigma_mean > avg_ratio_cut ] , avg_sigma[ avg_ratio_sigma_mean > avg_ratio_cut ], ".")        
        plt.yscale('log')
        plt.xscale('log')
        plt.xlabel("BLM Offset [Gy/s]")
        plt.ylabel("BLM Std Deviation [Gy/s]")
        
        

        ## Add to df the calculated quantitites
        #print(avg_sigma)
        df["Avg_Mean"]  = avg_mean
        df["Avg_Sigma"] = avg_sigma
        #print(df["Avg_Sigma"])
        

        # Reduce the DataFrame including only selected blm in the blmList
        if len(blmList) != 0 : df = df[ [ i in blmList for i in df["BlmMonitor"] ] ]
        #print("Reduce DF size if blmList")

        # BLN: verify this!
        # BLN: What do we do wiht the DUMP and TL ?
        df_ic = df[ [ self._getBlmType(blm)  == "IC" for blm in df["BlmMonitor"] ] ]
        df_lic = df[ [ self._getBlmType(blm) == "LIC" for blm in df["BlmMonitor"] ] ]
        df_dump_ic = df[ [ self._getBlmType(blm) == "DUMP_IC" for blm in df["BlmMonitor"] ] ]
        df_dump_lic = df[ [ self._getBlmType(blm) == "DUMP_LIC" for blm in df["BlmMonitor"] ] ]
        
        


        # PLOT BLM vs SPOS
        #font = {'family' : 'DejaVu Sans','weight' : 'normal','size'   : 16}
        #matplotlib.rc('font', **font)
        self.fig_size = (15,6)
        self.fig_counter += 1
        plt.figure(self.fig_counter, self.fig_size)
        #fig, axs = plt.subplots(2,2,sharex=True,gridspec_kw={'hspace': 0})
        #fig, axs = plt.subplots(2,2,sharex=True, figsize = self.fig_size , gridspec_kw={'hspace': 0})
        #fig.suptitle("From %s to %s for RS%02d" %(t1.split()[0], t2.split()[0], self.RS ))

        # OFFSET
        if len(df_ic) != 0:
            plt.errorbar(df_ic["BlmDcum"], df_ic["Avg_Mean"], yerr = df_ic["Avg_Sigma"], capsize = 4, fmt="ob", markersize=2, label="IC")
            #plt.errorbar(df_ic["BlmDcum"], df_ic["Avg_Mean"], capsize = 4, fmt="ob", markersize=2, label="IC")
        #if len(df_lic) != 0:
        #    axs[0,0].errorbar(df_lic["BlmDcum"], df_lic["Avg_Mean"], yerr = df_lic["Avg_Sigma"], capsize = 4, fmt="og",  markersize=2,label="LIC")
        if len(df_dump_ic) != 0:
            plt.errorbar(-(df_dump_ic["BlmDcum"]), df_dump_ic["Avg_Mean"], yerr = df_dump_ic["Avg_Sigma"], capsize = 4, fmt="or",  markersize=2,label="DUMP IC")
            #plt.errorbar(-(df_dump_ic["BlmDcum"]), df_dump_ic["Avg_Mean"], capsize = 4, fmt="or",  markersize=2,label="DUMP IC")
        #if len(df_dump_lic) !=0:
        #    axs[0,0].errorbar(-(df_dump_lic["BlmDcum"]), df_dump_lic["Avg_Mean"], yerr = df_dump_lic["Avg_Sigma"], capsize = 4, fmt="ok",  markersize=2,label="DUMP LIC")
        plt.ylabel("BLM Offset [Gy/s]")
        plt.xlabel("Position in the LHC [m]")
        plt.ticklabel_format(axis='y', style='sci', scilimits=(-3,3))
        plt.yscale('log')
        

        self.fig_size = (15,6)
        self.fig_counter += 1
        plt.figure(self.fig_counter, self.fig_size)


        # SIGMA 
        if len(df_ic) != 0:
            plt.errorbar(df_ic["BlmDcum"], df_ic["Avg_Sigma"], capsize = 4, fmt="ob",  markersize=2,label="IC", picker=True)
        #if len(df_lic) != 0:
        #    axs[1,0].errorbar(df_lic["BlmDcum"], df_lic["Avg_Sigma"], capsize = 4, fmt="og",  markersize=2,label="LIC")
        if len(df_dump_ic) != 0:
            plt.errorbar(-(df_dump_ic["BlmDcum"]), df_dump_ic["Avg_Sigma"], capsize = 4, fmt="or",  markersize=2,label="DUMP IC")
        #if len(df_dump_lic) != 0:
        #    axs[1,0].errorbar(-(df_dump_lic["BlmDcum"]), df_dump_lic["Avg_Sigma"], capsize = 4, fmt="ok",  markersize=2,label="DUMP LIC")
        plt.ylabel('BLM Std Deviation [Gy/s]')
        plt.xlabel("Position in the LHC [m]")
        plt.ticklabel_format(axis='y', style='sci', scilimits=(-3,3))
        plt.yscale('log')

        plt.show()

        return

        
if __name__ == "__main__":
    #obj = BlmNoise( rs = 9, blmTypes=["IC"])
    obj = BlmNoise( rs = 9)
    obj.ShowPlots = False
    


