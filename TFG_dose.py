import os, sys
import pytimber
from blmdose import BlmDose
import matplotlib.pyplot as plt
import matplotlib
db = pytimber.LoggingDB()
import blmtime

t1 = "2015-04-15 00:00:00"
t2 = "2018-12-20 00:00:00"


t1 = "2018-12-01 00:00:00"
obj=BlmDose()
obj.output_dir_path = "../Dose_201"
obj.output_files = "../TFG"

#listfiles, dates = obj._getBlmDoseFileList_TFG(t1, t2)

#print(listfiles)
obj.ShowPlots = True
#obj.plotDoseAlongRing(t1,t2)
blm_5 = ["BLMTI.04L5.B2E10_TANC.4L5", "BLMTI.04R5.B1E10_TANC.4R5"]
blm_1 = ["BLMTI.04L1.B2E10_TANAL.4L1", "BLMTI.04R1.B1E10_TANAR.4R1"]
blm_7 = ["BLMTI.06L7.B2I10_TCLA.A6L7.B2", "BLMTI.06R7.B1E10_TCLA.A6R7.B1"]
blm_7 = ["BLMTI.06L7.B2I10_TCLA.A6L7.B2", "BLMEI.06L7.B1E10_TCSM.A6L7.B1"]

obj.plotDoseVsKeys(t1,t2,keyPlot = "Lumi", keyCumulate = True, blmList = blm_5)
#obj.plotDoseVsKeys(t1,t2,keyPlot = "Int", keyCumulate = True, blmList = blm_7)
