import blmnoise
import blmtime

# MANY blm MIGHT NOT EXIST...

#t0 = "2016-05-01 00:00:00" # + 230 days
t0 = "2016-03-25 00:00:00" # 
#t0 = "2016-09-17 00:00:00" # 
t_final = "2016-05-01 00:00:00" # 
for i in range(40):
#for i in range(2):
    t1 = blmtime.strFromTime( blmtime.timeFromStr( t0 ) + i*24*3600)
    t2 = blmtime.strFromTime( blmtime.timeFromStr( t0 ) + (i+1)*24*3600)
    if blmtime.timeFromStr(t2) > blmtime.timeFromStr(t_final):
        sys.exit()
    print("***************************")
    print(t1, t2)
    obj = blmnoise.BlmNoise( rs = 9, beamModes=["INJPROT", "NOBEAM", "SETUP","CYCLING"])
    obj.ShowPlots = False
    obj.output_dir_path = "../BlmNoiseDataCorrected_2016"
    obj.useVectorNumericBlm = True 
    obj.runNoiseAnalysisDuringPeriod( t1, t2 )
    print("***************************\n")

"""
t1 = "2016-09-17 00:00:00"
t2 = "2016-09-18 00:00:00"

obj = blmnoise.BlmNoise( rs = 9, beamModes=["INJPROT", "NOBEAM", "SETUP","CYCLING"])
obj.output_dir_path = "../BlmNoiseDataCorrected_2016"
obj.ShowPlots = True
obj.plotNoiseAlongRing( t1, t2)
"""
## Raros en 2016-05-07 creo 'BLMBI.26L1.B0T10_MBB-MBA_25L1' 'BLMQI.25L1.B2E30_MQ'
## 'BLMQI.25L1.B1I10_MQ' 'BLMQI.25L1.B2E10_MQ' 'BLMQI.25L1.B1I30_MQ'
## 'BLMBI.25L1.B0T20_MBB-MBA_25L1'
