#!/usr/bin/env python3
# -*- coding: utf-8 -*-
# auth: B.Salvachua
# date: 2019-11-08
# Generates a CSV file with the information on BLM from LSA DB by defaul
# In LSA table:


import os
os.environ['ORACLE_HOME'] = "/afs/cern.ch/project/oracle/linux/prod"
os.environ['TNS_ADMIN']   = "/etc/tnsnames.ora"
import dbsetup 
import cx_Oracle, configparser
import datetime
import pandas 


## get_blm_db_info_tn ONLY works from TN !!
# use table blm_data_for_lsa_mv from LAYOUT_BLM
# For the threshodls db_lsa
def get_blm_layout_info_tn( output_dir = "./"):
    # Only works in the Technical Network
    # Connect to DB
    username = dbsetup.db_layout.USER
    passw = dbsetup.db_layout.PASSW
    server = dbsetup.db_layout.SERVER

    print(username, passw, server )
    db = cx_Oracle.connect(username, passw, server )
    print('Connected to ',db.dsn,' as user:',db.username)
    cur = db.cursor()
    # Check the name of the tables #############################
    #query = "SELECT * from all_tables"
    #cur.execute(query)
    #for row in cur.description:
    #    print(row[0]) # second row is the TALBE_NAME (ELEMENT [1] IN THE cursor)
    #for row in cur:
    #    if "BLM_DATA" in row[1]: print(row[0], row[1])
    #############################################################   
    query = "SELECT * from BLM_DATA_FOR_LSA_MV order by BLM_DCUM"
    cur.execute(query)
    header = [ row[0] for row in cur.description ]
    blmInfoDF = pandas.DataFrame( list(cur), columns = header )
    current_date = datetime.datetime.now().strftime("%Y%m%d")
    if not os.path.exists(output_dir): os.makedirs(output_dir)
    filename = output_dir+"/"+current_date+"_BlmDbLayout.csv"
    blmInfoDF.to_csv( filename, sep=",", index = False)
    cur.close()
    db.close()
    return

def get_blm_lsa_info_tn( output_dir = "./"):
    # Only works in the Technical Network
    # Connect to DB
    username = dbsetup.db_lsa.USER
    passw = dbsetup.db_lsa.PASSW
    server = dbsetup.db_lsa.SERVER
    
    print(username, passw, server )
    db = cx_Oracle.connect(username, passw, server )
    print('Connected to ',db.dsn,' as user:',db.username)
    cur = db.cursor()
    # Check the name of the tables #############################
    query = "SELECT * from all_tables"
    cur.execute(query)
    tables = list(cur)
    #for row in cur.description:
    #    print(row[0]) # second row is the TABLE_NAME (ELEMENT [1] IN THE cursor) OWNER is ELEMENT[0]
    #for row in tables:
    #    if "BLM" in row[1] and "LSA" == row[0]:
    #        print(row[0], row[1])
    #        query = "SELECT * from "+row[1]
    #        cur.execute(query)
    #        print("****************")
    #        [print(row[0]) for row in cur.description ]
    #        print("****************")
    ##############################################################   

    #query = "SELECT * from LSA.BLM_INFO inner join LSA.BLM_CRATE_CARDS on LSA.BLM_CRATE_CARDS.CRATE_CMW_NAME = LSA.BLM_INFO.CRATE_CMW_NAME and LSA.BLM_CRATE_CARDS.CRATE_CARD_IDX = LSA.BLM_INFO.CRATE_CARD_IDX"
    #query = "SELECT * from LSA.BLM_INFO inner join LSA.BLM_CRATE_CARDS using(CRATE_CMW_NAME,CRATE_CARD_IDX) inner join LSA.BLM_CRATES using(CRATE_CMW_NAME) order by DCUM"
    #query = "SELECT * from LSA.BLM_FAMILY_THRESHOLDS"
    #BLM_MASTER_THRESHOLDS for expert names
    
    
    list_columnes = ""
    list_columnes += "LSA.BLM_APPLIED_THRESHOLDS.MONITOR_OFFICIAL_NAME,"
    list_columnes += "LSA.BLM_APPLIED_THRESHOLDS.MONITOR_EXP_NAME,"
    list_columnes += "LSA.BLM_APPLIED_THRESHOLDS.FAMILY_NAME,"
    list_columnes += "LSA.BLM_APPLIED_THRESHOLDS.DCUM,"
    list_columnes += "LSA.BLM_APPLIED_THRESHOLDS.IP,"
    list_columnes += "LSA.BLM_APPLIED_THRESHOLDS.MONITOR_FACTOR,"
    list_columnes += "LSA.BLM_APPLIED_THRESHOLDS.CONV_FACTOR_GY_S_AMPS,"
    list_columnes += "LSA.BLM_APPLIED_THRESHOLDS.CONV_FACTOR_BLMBIT_GY_S,"
    list_columnes += "LSA.BLM_APPLIED_THRESHOLDS.CRATE_CMW_NAME,"
    list_columnes += "LSA.BLM_APPLIED_THRESHOLDS.DAB_INDEX,"
    list_columnes += "LSA.BLM_APPLIED_THRESHOLDS.CHANNEL_INDEX,"
    list_columnes += "LSA.BLM_APPLIED_THRESHOLDS.BLECF_SERIAL,"
    list_columnes += "LSA.BLM_APPLIED_THRESHOLDS.BLETC_FIRM_V,"
    list_columnes += "LSA.BLM_APPLIED_THRESHOLDS.IS_MASKED,"
    list_columnes += "LSA.BLM_APPLIED_THRESHOLDS.IS_CONNECTED_TO_BIS,"
    list_columnes += "LSA.BLM_APPLIED_THRESHOLDS.IS_CABLE_CONNECTED,"
    list_columnes += "LSA.BLM_APPLIED_THRESHOLDS.DEVICE_ID"
    query = "SELECT distinct "+list_columnes+" from LSA.BLM_APPLIED_THRESHOLDS order by DCUM"
    cur.execute(query)

    #print("****************")
    #[print(row[0]) for row in cur.description ]
    #return
    header = [ row[0] for row in cur.description ]
    blmInfoDF = pandas.DataFrame( list(cur), columns = header )

    current_date = datetime.datetime.now().strftime("%Y%m%d")
    if not os.path.exists(output_dir): os.makedirs(output_dir)
    filename = output_dir+"/"+current_date+"_BlmDbLsa.csv"
    blmInfoDF.to_csv( filename, sep=",", index = False)
    cur.close()
    db.close()
    return


def get_blm_lsa_info_from_file( filename = "lsa/20200213_BlmDbLsa.csv", user_blm_list = [], blm_types= ["IC","SEM","LIC"], filter_data = True):
    ## Add script to crate this file
    df = pandas.read_csv(filename,encoding='utf-7') 
    if filter_data == False: return df
    filterList = [ is_blm_good( blm_name , user_blm_list = user_blm_list, blm_types = blm_types) for blm_name in df["MONITOR_EXP_NAME"].values ]
    df = df[ filterList ]
    df["DCUM"] = df["DCUM"]/100. # in meters
    return df

def get_blm_lsa_name_dcum_from_file(filename = "lsa/20200213_BlmDbLsa.csv",user_blm_list = [], blm_types= ["IC","SEM","LIC"], filter_data = True):
    df = get_blm_lsa_info_from_file( filename, user_blm_list, blm_types, filter_data)
    return df["MONITOR_EXP_NAME"].values, df["DCUM"].values



def filter_blm( blm_name_list, blm_dcum_list ,user_blm_list = [], blm_types=["IC","SEM","LIC"] ):
    blm_name = []
    blm_dcum = []
    for b in range( len(blm_name_list)):
        if is_blm_good( blm_name_list[b] , user_blm_list = user_blm_list, blm_types = blm_types):
            blm_name += [ blm_name_list[b] ]
            blm_dcum += [ blm_dcum_list[b] ]
    return blm_name, blm_dcum

def is_blm_good(blm_name, user_blm_list = [], blm_types=["IC","SEM","LIC"] ): 
     badBlmNames = ["BLMM","BLMCC"]
     blmIC  = ["BLMEI", "BLMQI", "BLMBI", "BLMAI", "BLMTI", "BLMDI"]
     blmLIC = ["BLMEL", "BLMQL", "BLMBL", "BLMAL", "BLMTL", "BLMDL"]
     blmSEM = ["BLMES", "BLMQS", "BLMBS", "BLMAS", "BLMTS", "BLMDS"]                
     if "SEM" not in blm_types:
         badBlmNames = badBlmNames + blmSEM
     if "IC" not in blm_types:
         badBlmNames = badBlmNames + blmIC
     if "LIC" not in blm_types:
         badBlmNames = badBlmNames + blmLIC

     # print(badBlmNames)
     for i in badBlmNames:
         if i in blm_name:
             return False

     if "BLM" not in blm_name: 
         return False

     #If a user_blm_list is provided use it
     if len(user_blm_list) !=0 :
         for iname in user_blm_list:
             if iname in blm_name: return True
         return False

     return True



if __name__ == "__main__":
    #get_blm_layout_info_tn("./layout")
    get_blm_lsa_info_tn("./lsa")    

    #blmname, blmdcum= get_blm_lsa_name_dcum_from_file(filename = "lsa/20200213_BlmDbLsa.csv" ,blm_types = ["LIC"])
    #get_blm_lsa_info_from_file(filename = "lsa/20200213_BlmDbLsa.csv")
