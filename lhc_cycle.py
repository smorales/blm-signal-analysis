import os, sys
import pytimber
from blmdata import BlmData
import matplotlib.pyplot as plt
import matplotlib
import numpy as np
db = pytimber.LoggingDB()
import blmtime
#from blmnoise import BlmNoise

#fills = db.getLHCFillData(7018)
#fills = db.getLHCFillData(7008)
fills = db.getLHCFillData(7006) #bastante bien
#fills = db.getLHCFillData(7237)
#print(fills)
#fills = db.getLHCFillData(7026)
#fills = db.getLHCFillData(7020)
#sys.exit()

obj = BlmData()

variables = ["LHC.BCTFR.A6R4.B1:BEAM_INTENSITY","LHC.BCTFR.A6R4.B2:BEAM_INTENSITY","LHC.BSRA.US45.B1:ABORT_GAP_ENERGY","LHC.BSRA.US45.B2:ABORT_GAP_ENERGY", "BLMTI.06L7.B1E10_TCP.D6L7.B1:LOSS_RS09","HX:SMP1_PRESENT","BLMTI.04L1.B1I10_TCTPH.4L1.B1:LOSS_RS09","BLMTI.04R1.B1E10_TANAR.4R1:LOSS_RS09","BLMTI.04L1.B2E10_TANAL.4L1:LOSS_RS09","CFV-SR1-BLMC:SYS_UNDER_TEST","HX:SMP2_PRESENT","BLMAI.10L1.B2E21_MBB:LOSS_RS09","BLMAI.11L1.B2E22_MBB:LOSS_RS09", "ATLAS:LUMI_TOT_INST", "CMS:LUMI_TOT_INST", "BLMTI.04L5.B2E10_TANC.4L5:LOSS_RS09"]
variables_2 = ["BLMTI.04L1.B2E10_TANAL.4L1","BLMAI.10L1.B2E21_MBB","BLMAI.11L1.B2E22_MBB"]


# para 7006 
#data = db.getAligned(variables, fills["startTime"]-800,fills["endTime"]+1500,master = variables[0], unixtime=True)

#data = db.getAligned(variables, fills["startTime"],fills["endTime"],master = variables[0], unixtime=True)

	
#for i in range(len(data["CFV-SR1-BLMC:SYS_UNDER_TEST"])):
#    if data["CFV-SR1-BLMC:SYS_UNDER_TEST"][i]==1:
#        print(blmtime.strFromTime(fills["startTime"]+i))
        
#sys.exit()
#print(data["LHC.BCTFR.A6R4.B1:BEAM_INTENSITY"][0])

#t1 = blmtime.strFromTime(fills["startTime"]-800)
#t2 = blmtime.strFromTime(fills["endTime"]+1500)
t1 = "2018-08-01 14:22:00"
t2 = "2018-08-01 15:42:00"


for i in fills["beamModes"]:
    if i["mode"] == "STABLE":
        line_1 = i["startTime"]
        t1 = blmtime.strFromTime(i["startTime"])
        t2 = blmtime.strFromTime(i["endTime"])
    #if i["mode"] == "RAMP":
    #    t1 = blmtime.strFromTime(i["startTime"])
    #    line_2 = i["endTime"]
    #if i["mode"] == "SQUEEZE":
    #    line_4 = i["startTime"]
   #     line_3 = i["endTime"]

    
    
     #   line = i["startTime"]
     #t2 = blmtime.strFromTime(i["endTime"])


data = db.getAligned(variables, t1,t2,master = variables[0], unixtime=True)
#tt_2,vv,blmList = obj.getData(t1,t2, variables_2)


#print(vv[blm + ":LOSS_RS09"])

tt = data['timestamps']

font = {'family' : 'DejaVu Sans',
        'weight' : 'normal',
                'size'   : 16}

matplotlib.rc('font', **font)

fig, ax1 = plt.subplots()
#fig.subplots_adjust(right=0.75)


def make_patch_spines_invisible(ax):
    ax.set_frame_on(True)
    ax.patch.set_visible(False)
    for sp in ax.spines.values():
        sp.set_visible(False)

#lns1=ax1.plot(tt, data["LHC.BCTFR.A6R4.B1:BEAM_INTENSITY"], 'b',label="Intensity Beam 1")
#lns2=ax1.plot(tt, data["LHC.BCTFR.A6R4.B2:BEAM_INTENSITY"], 'r',label="Intensity Beam 2")

lns1 = ax1.plot(tt, data["BLMTI.04L5.B2E10_TANC.4L5:LOSS_RS09"], 'r',label="BLM Signal")
#lns2 = ax1.plot(tt, data["BLMTI.04R1.B1E10_TANAR.4R1:LOSS_RS09"], 'b',label="BLM Signal")
#lns1 = ax1.plot(tt_2, vv["BLMTI.04L1.B2E10_TANAL.4L1:LOSS_RS09"], 'b',label="BLM Signal")
#lns1 = ax1.plot(tt_2, vv["BLMAI.10L1.B2E21_MBB:LOSS_RS09"], 'b',label="BLM Signal")
#lns2 = ax1.plot(tt_2, vv["BLMAI.11L1.B2E22_MBB:LOSS_RS09"], 'g',label="BLM Signal")
ax2 = ax1.twinx()
#lns2 = ax2.plot(tt, data["HX:SMP2_PRESENT"], 'g',label="Beam present" )
#lns3 = ax2.plot(tt, data["CFV-SR1-BLMC:SYS_UNDER_TEST"], 'r',label="Beam present" )
#lns2 = ax2.plot(tt, data["LHC.BSRA.US45.B1:ABORT_GAP_ENERGY"], 'g',label="Energy Beam 1" )
lns3 = ax2.plot(tt, np.multiply(data["CMS:LUMI_TOT_INST"],1e-9), 'g',label="Energy Beam 2" )
#lns4 = ax2.plot(tt, np.multiply(data["ATLAS:LUMI_TOT_INST"],1e-9), 'm',label="Energy Beam 2" )
#ax3 = ax1.twinx()

#ax3.spines["right"].set_position(("axes", 1.08))
#make_patch_spines_invisible(ax3)
# Second, show the right spine.
#ax3.spines["right"].set_visible(True)
#lns3=ax3.plot(tt,data["HX:SMP2_PRESENT"],'m',label="Beam present")
#lns3=ax3.plot(tt,data["LHC.BCTFR.A6R4.B1:BEAM_INTENSITY"],'b',label="Intensity Beam 1")

#lns = lns1 + lns2  
#labs = [l.get_label() for l in lns]
#plt.legend(lns, labs, loc="best")

ax1.set_ylabel("BLM Signal [Gy/s]")
#ax1.set_title("BLM signal for the monitor BLMEI.05R7.B1E10_TCSM.A5R7.B1:LOSS_RS09 with RS09")
#ax1.set_yscale("log")

#t_linea = blmtime.timeFromStr("2018-08-01 15:02:50")
#plt.legend(loc=2)
ax2.set_ylabel("CMS instant luminosity [fb$^{-1}$ s$^{-1}$]")
#ax3.set_ylabel("Beam present")
#ax2.set_ylabel("Beam present / Connectivity test running")
#ax3.vlines(line, -0.01,1.01,linestyles='dashed')
#plt.tight_layout()
plt.ticklabel_format(axis='y', style='sci', scilimits=(-3,3))

#for i in fills["beamModes"]:
    #ax2.vlines(i['startTime'],min( data["LHC.BSRA.US45.B1:ABORT_GAP_ENERGY"]), max(data["LHC.BSRA.US45.B1:ABORT_GAP_ENERGY"]),linestyles='dashed')
    #ax2.text(i['startTime'], (max(data["LHC.BSRA.US45.B1:ABORT_GAP_ENERGY"])-min(data["LHC.BSRA.US45.B1:ABORT_GAP_ENERGY"]))/2,i['mode'],rotation=90, va='center')
#ax2.vlines(line_1,min( data["LHC.BSRA.US45.B2:ABORT_GAP_ENERGY"]), max(data["LHC.BSRA.US45.B2:ABORT_GAP_ENERGY"]),linestyles='dashed')
#ax2.vlines(line_2,min( data["LHC.BSRA.US45.B2:ABORT_GAP_ENERGY"]), max(data["LHC.BSRA.US45.B2:ABORT_GAP_ENERGY"]),linestyles='dashed')
#ax2.vlines(line_3,min( data["LHC.BSRA.US45.B2:ABORT_GAP_ENERGY"]), max(data["LHC.BSRA.US45.B2:ABORT_GAP_ENERGY"]),linestyles='dashed')
#ax2.vlines(line_4,min( data["LHC.BSRA.US45.B2:ABORT_GAP_ENERGY"]), max(data["LHC.BSRA.US45.B2:ABORT_GAP_ENERGY"]),linestyles='dashed')
#ax1.ticklabel_format(axis='y', style='sci', scilimits=(-3,3))
pytimber.set_xaxis_date()

print("hola")

plt.show()
